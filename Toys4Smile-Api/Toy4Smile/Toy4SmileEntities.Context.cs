﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Toy4Smile
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class Toy4SmileEntities : DbContext
    {
        public Toy4SmileEntities()
            : base("name=Toy4SmileEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Donation> Donations { get; set; }
        public virtual DbSet<OrphanReceiving> OrphanReceivings { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<TrackingNumber> TrackingNumbers { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<image> images { get; set; }
    
        public virtual ObjectResult<searchDonation_Result> searchDonation(string trackingNo)
        {
            var trackingNoParameter = trackingNo != null ?
                new ObjectParameter("trackingNo", trackingNo) :
                new ObjectParameter("trackingNo", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<searchDonation_Result>("searchDonation", trackingNoParameter);
        }
    
        public virtual ObjectResult<sp_GetAllDonations_Result> sp_GetAllDonations()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAllDonations_Result>("sp_GetAllDonations");
        }
    
        public virtual ObjectResult<sp_getAllDonationsByUserId_Result> sp_getAllDonationsByUserId(Nullable<int> userId)
        {
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("userId", userId) :
                new ObjectParameter("userId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_getAllDonationsByUserId_Result>("sp_getAllDonationsByUserId", userIdParameter);
        }
    
        public virtual int sp_InsertDonationNew(string firstName, string countrycode, string email, string address, Nullable<int> roleId, string noOfToysDonated, string trackingIdD, string status, Nullable<System.DateTime> dateOfDonation, string trackingNumber, string number)
        {
            var firstNameParameter = firstName != null ?
                new ObjectParameter("firstName", firstName) :
                new ObjectParameter("firstName", typeof(string));
    
            var countrycodeParameter = countrycode != null ?
                new ObjectParameter("countrycode", countrycode) :
                new ObjectParameter("countrycode", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("email", email) :
                new ObjectParameter("email", typeof(string));
    
            var addressParameter = address != null ?
                new ObjectParameter("address", address) :
                new ObjectParameter("address", typeof(string));
    
            var roleIdParameter = roleId.HasValue ?
                new ObjectParameter("RoleId", roleId) :
                new ObjectParameter("RoleId", typeof(int));
    
            var noOfToysDonatedParameter = noOfToysDonated != null ?
                new ObjectParameter("noOfToysDonated", noOfToysDonated) :
                new ObjectParameter("noOfToysDonated", typeof(string));
    
            var trackingIdDParameter = trackingIdD != null ?
                new ObjectParameter("TrackingIdD", trackingIdD) :
                new ObjectParameter("TrackingIdD", typeof(string));
    
            var statusParameter = status != null ?
                new ObjectParameter("status", status) :
                new ObjectParameter("status", typeof(string));
    
            var dateOfDonationParameter = dateOfDonation.HasValue ?
                new ObjectParameter("dateOfDonation", dateOfDonation) :
                new ObjectParameter("dateOfDonation", typeof(System.DateTime));
    
            var trackingNumberParameter = trackingNumber != null ?
                new ObjectParameter("TrackingNumber", trackingNumber) :
                new ObjectParameter("TrackingNumber", typeof(string));
    
            var numberParameter = number != null ?
                new ObjectParameter("Number", number) :
                new ObjectParameter("Number", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_InsertDonationNew", firstNameParameter, countrycodeParameter, emailParameter, addressParameter, roleIdParameter, noOfToysDonatedParameter, trackingIdDParameter, statusParameter, dateOfDonationParameter, trackingNumberParameter, numberParameter);
        }
    
        public virtual ObjectResult<sp_Recent_Donations_Result> sp_Recent_Donations()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_Recent_Donations_Result>("sp_Recent_Donations");
        }
    
        public virtual ObjectResult<sp_trackingNumber_Result> sp_trackingNumber(Nullable<System.DateTime> date)
        {
            var dateParameter = date.HasValue ?
                new ObjectParameter("date", date) :
                new ObjectParameter("date", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_trackingNumber_Result>("sp_trackingNumber", dateParameter);
        }
    
        public virtual ObjectResult<spGetDonationById_Result> spGetDonationById(Nullable<int> donationId)
        {
            var donationIdParameter = donationId.HasValue ?
                new ObjectParameter("donationId", donationId) :
                new ObjectParameter("donationId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetDonationById_Result>("spGetDonationById", donationIdParameter);
        }
    }
}
