﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using Toy4Smile.Models;
using Toy4Smile.Models.ViewModels;

namespace Toy4Smile.Controllers
{
    public class DonationsController : ApiController
    {
        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donations/getDonationsByUserId/{id}")]
        public List<Donation> getDonationsByUserId(int id)
        {
            List<Donation> d = new List<Donation>();
            Toy4SmileEntities t = new Toy4SmileEntities();
            d = t.Donations.Where(x => x.UserId == id).ToList();
            return d;
        }

        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donations/getAllDonations")]
        public List<Donation> getAllDonations()
        {
            List<Donation> d = new List<Donation>();
            Toy4SmileEntities t = new Toy4SmileEntities();
            d = t.Donations.ToList();
            return d;
        }

        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donations/getAllDonationsByStatus/{status}")]
        public List<Donation> getAllDonationsByStatus(string status)
        {
            List<Donation> d = new List<Donation>();
            Toy4SmileEntities t = new Toy4SmileEntities();
            d = t.Donations.Where(x => x.status == status).ToList();
            return d;
        }

        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donations/getAllReceivedDonationsForOrphanage/{status}")]
        public List<Donation> getAllReceivedDonationsForOrphanage(string status)
        {
            List<Donation> d = new List<Donation>();
            Toy4SmileEntities t = new Toy4SmileEntities();
            List<string> stringList = status.Split(',').ToList();
            string donationStatus = stringList[0];
            int userId = Convert.ToInt32(stringList[1]);
            d = t.Donations.Where(x => x.status == donationStatus && x.OrphanageId == userId).ToList();
            return d;
        }

        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donations/getAllDonationsByOrphanageId/{id}")]
        public List<Donation> getAllDonationsByOrphanageId(int id)
        {
            List<Donation> d = new List<Donation>();
            Toy4SmileEntities t = new Toy4SmileEntities();
            d = t.Donations.Where(x => x.OrphanageId == id && x.status != "Received").ToList();
            return d;
        }

        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donations/getDonationByTrackingId/{trackingId}")]
        public Donation getDonationByTrackingId(string trackingId)
        {
            Donation d = new Donation();
            Toy4SmileEntities t = new Toy4SmileEntities();
            d = t.Donations.Where(x => x.TrackingId == trackingId).FirstOrDefault();
            return d;
        }

        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donations/getAllDonationsByTrackingId/{trackingId}")]
        public Donation getAllDonationsByTrackingId(string trackingId)
        {
            Donation d = new Donation();
            Toy4SmileEntities t = new Toy4SmileEntities();
            List<string> stringList = trackingId.Split(',').ToList();
            string searchedTrackingId = stringList[0];
            int userId = Convert.ToInt32(stringList[1]);
            d = t.Donations.Where(x => x.TrackingId == searchedTrackingId && x.OrphanageId == userId).FirstOrDefault();
            return d;
        }

        [HttpPost]
        [Route("api/Donations/updateDonationStatus")]
        public Donation updateDonationStatus(Donation value)
        {
            Donation d = new Donation();
            Toy4SmileEntities t = new Toy4SmileEntities();
            var dd = t.Donations.First(x => x.DonationId == value.DonationId);
            dd.status = value.status;
            t.SaveChanges();
            return d;
        }

        [HttpPost]
        [Route("api/Donations/deleteDonation")]
        public string deleteDonation(Donation value)
        {
            string status = "";
            try
            {
                status = value.status;
                Donation d = new Donation();
                Toy4SmileEntities t = new Toy4SmileEntities();
                var dd = t.Donations.First(x => x.DonationId == value.DonationId);
                t.Donations.Remove(dd);
                t.SaveChanges();
                return status;
            }
            catch (Exception ex)
            {
                return status;
            }
        }

        [HttpGet]
        [Route("api/Donations/getDonationByDonatoinId/{donationId}")]
        public donationByIdViewModel getDonationByDonatoinId(int donationId)
        {
            donationByIdViewModel d = new donationByIdViewModel();
            Toy4SmileEntities t = new Toy4SmileEntities();
            var dd = t.spGetDonationById(donationId);
            foreach (var item in dd)
            {
                d.dateOfDonation = Convert.ToDateTime(item.dateOfDonation);
                d.DonationId = item.DonationId;
                d.specialComments = item.SpecialComments;
                d.orphanageId = item.OrphanageId;
                d.email = item.email;
                d.firstName = item.firstName;
                d.lastName = item.lastName;
                d.noOfToysDonated = item.noOfToysDonated;
                d.status = item.status;
                d.TrackingId = item.TrackingId;
                d.TrackingNumber = item.TrackingNumber;
                d.address = item.address;
                d.mobile = item.mobile;
                d.countrycode = item.countrycode;
                d.UserId = Convert.ToInt32(item.UserId);
            }
            return d;
        }

        [HttpGet]
        [Route("api/Donations/getDonationImagesByDonationId/{donationId}")]
        public List<imagePathss> getDonationImagesByDonationId(int donationId)
        {
            donationByIdViewModel d = new donationByIdViewModel();
            Toy4SmileEntities t = new Toy4SmileEntities();
            List<imagePathss> imagePath = new List<imagePathss>();
            var images = t.images.Where(x => x.DonationId == donationId);
            foreach (var item in images)
            {
                imagePathss imageP = new imagePathss();
                imageP.imageid = item.ImageId;
                imageP.imagePath = imageP.imagePath;
                imageP.imagePath = item.imageName;
                imagePath.Add(imageP);
            }
            return imagePath;
        }

        [HttpPost]
        [Route("api/Donations/deleteimage")]
        public Boolean deleteimage(imagePathss img_obj)
        {
            try
            {
                Donation d = new Donation();
                Toy4SmileEntities t = new Toy4SmileEntities();
                var md = t.images.First(x => x.ImageId == img_obj.imageid);
                t.images.Remove(md);
                t.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
