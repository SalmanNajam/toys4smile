﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Web.Http;
using Toy4Smile.Models.ViewModels;
using System;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.IO.Compression;
using Microsoft.SqlServer.Server;

namespace Toy4Smile.Controllers
{
    public class DonorController : ApiController
    {
        public static void Compress(FileInfo fi)
        {
            // Get the stream of the source file.
            using (FileStream inFile = fi.OpenRead())
            {
                // Prevent compressing hidden and 
                // already compressed files.
                if ((File.GetAttributes(fi.FullName)
                    & FileAttributes.Hidden)
                    != FileAttributes.Hidden & fi.Extension != ".gz")
                {
                    // Create the compressed file.
                    using (FileStream outFile =
                                File.Create(fi.FullName + ".gz"))
                    {
                        using (GZipStream Compress =
                            new GZipStream(outFile,
                            CompressionMode.Compress))
                        {
                            // Copy the source file into 
                            // the compression stream.
                            inFile.CopyTo(Compress);

                            Console.WriteLine("Compressed {0} from {1} to {2} bytes.",
                                fi.Name, fi.Length.ToString(), outFile.Length.ToString());
                        }
                    }
                }
            }
        }

        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/GetDonorClaims")]
        [Authorize]
        public UserViewModel GetDonorClaims()
        {
            DateTime dobClaim = DateTime.Now;
            var identityClaims = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identityClaims.Claims;

            UserViewModel uvm = new UserViewModel()
            {
                UserId = Convert.ToInt32(identityClaims.FindFirst("Userid").Value),
                firstName = identityClaims.FindFirst("Firstname").Value,
                email = identityClaims.FindFirst("Email").Value,
                mobile = identityClaims.FindFirst("Mobile").Value,
                address = identityClaims.FindFirst("Address").Value,
                status = Convert.ToBoolean(identityClaims.FindFirst("Status").Value),
                RoleId = Convert.ToInt32(identityClaims.FindFirst("Roleid").Value),
                RoleName = identityClaims.FindFirst("RoleName").Value,
            };
            return uvm;
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/HasEmailRegistered")]
        public bool HasEmailRegistered(EmailModel email)
        {
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                User user = (from u in dal.Users where u.email == email.emailid select u).FirstOrDefault();
                if (user == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/GetUserByEmail")]
        public User GetUserByEmail(EmailModel email)
        {
            User user = new User();
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                user = (from u in dal.Users where u.email == email.emailid select u).FirstOrDefault();
                return user;
            }
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/ForgotPassword")]
        public bool ForgotPassword(EmailModel email)
        {
            string randomstring = "";
            string url = "http://www.toys4smile.com/#/signin";
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                User user = (from u in dal.Users where u.email == email.emailid select u).FirstOrDefault();
                if (user == null)
                {
                    return false;
                }
                else
                {
                    Guid randomnumber = System.Guid.NewGuid();
                    randomstring = randomnumber.ToString();
                    randomstring = randomstring.Substring(0, 8);
                    user.password = randomstring;
                    dal.SaveChanges();
                    //For to send an email at Gmail Starts: gmail ID: setting link: https://myaccount.google.com/lesssecureapps?pli=1
                    var fromAddress = new MailAddress("toys4smile@gmail.com", "Toys4Smile");
                    var toAddress = new MailAddress(user.email, "Toys4Smile");
                    const string fromPassword = "Toys1234";
                    const string subject = "Your password has been reset!";
                    string body = "Dear " + user.firstName + ", \r\n \r\n" +
                        "Your password has been reset to: " + randomstring +
                        "\r\n\r\nPlease sign in with your new password and change it after sign in.\r\n\r\nToys4Smile sign in link: " + url +
                        "\r\n \r\n \r\n \r\nRegards,\r\n Toys4Smile Team";
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                        Timeout = 20000
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(message);
                    }
                    //For to send an email at Gmail Ends
                    return true;
                }
            }
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/registerUser")]
        public bool registerUser(RegisterUserModel u)
        {
            try
            {
                Toy4SmileEntities t = new Toy4SmileEntities();
                User user = new User();
                user.firstName = u.firstName;
                user.lastName = u.lastName;
                user.mobile = u.mobileNo;
                user.email = u.email;
                user.dob = Convert.ToDateTime(u.dob);
                user.password = u.password;
                user.address = u.address;
                user.RoleId = 3;
                user.status = true;
                user.date = DateTime.Now;
                user.countrycode = u.countrycode;
                t.Users.Add(user);
                //t.SaveChanges();
                string url = "http://www.toys4smile.com/#/signin";
                //using (SmtpClient smtpClient = new SmtpClient())
                //{
                //    var basicCredential = new NetworkCredential("toyssmil", "98YEvge67s");
                //    using (MailMessage message = new MailMessage())
                //    {
                //        MailAddress fromAddress = new MailAddress("info@toys4smile.com");

                //        smtpClient.Host = "win03.tmd.cloud";
                //        smtpClient.UseDefaultCredentials = false;
                //        smtpClient.Credentials = basicCredential;

                //        message.From = fromAddress;
                //        message.Subject = "your subject";
                //        // Set IsBodyHtml to true means you can send HTML email.
                //        message.IsBodyHtml = true;
                //        message.Body = "<h1>your message body</h1>";
                //        message.To.Add("sa1man@msn.com");

                //        try
                //        {
                //            smtpClient.Send(message);
                //        }
                //        catch (Exception ex)
                //        {
                //            //Error, could not send the message
                //            int a;
                //        }
                //    }
                //}


                //try
                //{
                //    var message = new MailMessage();
                //    message.To.Add(new MailAddress("sa1man@msn.com"));  // replace with valid value 
                //    message.From = new MailAddress("info@toys4smile.com");  // replace with valid value
                //    message.Subject = "Your email subject";
                //    message.Body = "Body";
                //    message.IsBodyHtml = true;

                //    using (var smtp = new SmtpClient())
                //    {
                //        var credential = new NetworkCredential
                //        {
                //            UserName = "info@toys4smile.com",  // replace with valid value
                //            Password = "Toys1234"  // replace with valid value
                //        };
                //        smtp.Credentials = credential;
                //        smtp.Host = "win03.tmd.cloud";
                //        smtp.Port = 465;
                //        smtp.EnableSsl = true;
                //        smtp.SendMailAsync(message);
                //    }
                //}
                //catch (Exception)
                //{

                //    throw;
                //}

                //try
                //{
                //    MailMessage mail = new MailMessage();
                //    mail.To.Add("sa1man@msn.com");
                //    mail.From = new MailAddress("info@toys4smile.com");
                //    mail.Subject = "hello";
                //    string Body = "bla bla bla";
                //    mail.Body = Body;
                //    mail.IsBodyHtml = true;
                //    SmtpClient smtp = new SmtpClient();
                //    smtp.Host = "win03.tmd.cloud";
                //    smtp.Port = 587;
                //    smtp.UseDefaultCredentials = false;
                //    smtp.Credentials = new NetworkCredential("info@toys4smile.com", "Toys1234"); // Enter seders User name and password  
                //    //smtp.EnableSsl = true;
                //    smtp.Send(mail);
                //}
                //catch (Exception ex)
                //{

                //    throw;
                //}

                //try
                //{
                //    NetworkCredential login = new NetworkCredential("info@toys4smile.com", "Toys1234");

                //    System.Net.Mail.MailMessage email = new MailMessage();

                //    email.To.Add(new MailAddress("info@toys4smile.com"));
                //    email.From = new MailAddress("info@toys4smile.com");
                //    email.Subject = "Question";

                //    email.Body = "sajkdhksjadhksadkasjkhdashjksdkjdashkasd";

                //    SmtpClient client = new SmtpClient("win03.tmd.cloud", 25);
                //    //client.EnableSsl = true;
                //    client.UseDefaultCredentials = false;
                //    client.Credentials = login;
                //    client.Send(email);
                //}
                //catch
                //{
                //}



                //For to send an email at Gmail Starts: gmail ID: setting link: https://myaccount.google.com/lesssecureapps?pli=1
                var fromAddress = new MailAddress("info@toys4smile.com", "Toys4Smile");
                var toAddress = new MailAddress(user.email, "Toys4Smile");
                const string fromPassword = "Toys1234";
                const string subject = "Registration Successfull";
                string body = "Dear " + user.firstName + ", \r\n \r\n" +
                    "You have been registered successfully. You may click on below mentioned link to sign in as donor" +
                    "\r\nToys4smile Sign in link: " + url +
                    "\r\n\r\nYour credentials are: " +
                    "\r\nEmail: " + user.email +
                    "\r\nPassword: " + user.password +
                    "\r\n \r\nThank you for your registration. \r\n \r\n Regards,\r\n Toys4Smile Team";
                var smtp = new SmtpClient
                {
                    Host = "win03.tmd.cloud",
                    Port = 587,
                    //EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    Timeout = 20000
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                //For to send an email at Gmail Ends
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/donateAsGuest")]
        public string donateAsGuest()
        {
            string trackingid = "";
            string trackingNumber = "";
            try
            {
                var formFiles = HttpContext.Current.Request.Files;
                var formDataa = HttpContext.Current.Request.Form;
                Toy4SmileEntities t = new Toy4SmileEntities();
                DonationViewModel dv = new DonationViewModel();
                var ss = DateTime.Now;
                string year = ss.Year.ToString();
                string month = ss.Month.ToString();
                string day = ss.Day.ToString();

                //string connectionString = "Data Source=DESKTOP-V4VKSII\\SQLEXPRESS;Initial Catalog=Toy4Smile;Integrated Security=true";
                string connectionString = "data source=184.154.206.133; initial catalog=Toy4Smile;user id=toyssmil; password=98YEvge67s";
                logCollection logColl = new logCollection();
                using (SqlConnection connection =
                    new SqlConnection(connectionString))
                {
                    // Create the Command and Parameter objects.
                    SqlCommand command = new SqlCommand("sp_trackingNumber", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@date", SqlDbType.Date));
                    command.Parameters["@date"].Value = DateTime.Now;
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        var comlumExist = HasColumn(reader, "DonationId");
                        if (comlumExist == true)
                        {
                            while (reader.Read())
                            {
                                if (reader != null)
                                {
                                    var sss = reader["DonationId"];
                                    var trackingNumbers = Convert.ToInt32(reader["TrackingNumber"]);
                                    trackingNumbers = trackingNumbers + 1;
                                    trackingNumber = trackingNumbers.ToString();
                                }
                            }
                            var length = trackingNumber.Length;
                            if (length == 1)
                            {
                                trackingNumber.Insert(0, "000");
                                trackingNumber = "000" + trackingNumber;
                            }
                            else if (length == 2)
                            {
                                trackingNumber.Insert(0, "00");
                                trackingNumber = "00" + trackingNumber;
                            }
                            else if (length == 3)
                            {
                                trackingNumber.Insert(0, "0");
                                trackingNumber = "0" + trackingNumber;
                            }
                            trackingid = year + "-" + month + "-" + day + "-" + trackingNumber;
                        }
                        else
                        {
                            trackingid = year + "-" + month + "-" + day + "-" + "0001";
                            trackingNumber = "0001";
                        }
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                try
                {
                    foreach (string item in formFiles)
                    {
                        HttpPostedFile file = formFiles[item];
                        if (file.ContentLength > 0)
                        {
                            var dataUrl = file.FileName;
                            logColl.Add(new gridClass { name = dataUrl });
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                using (SqlConnection connection =
                   new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_InsertDonationNew", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@firstName", SqlDbType.NVarChar));
                        cmd.Parameters["@firstName"].Value = formDataa["fullName"];
                        cmd.Parameters.Add(new SqlParameter("@Number", SqlDbType.NVarChar));
                        cmd.Parameters["@Number"].Value = formDataa["mobileNumber"];
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.NVarChar));
                        cmd.Parameters["@email"].Value = formDataa["email"];
                        cmd.Parameters.Add(new SqlParameter("@countrycode", SqlDbType.NVarChar));
                        cmd.Parameters["@countrycode"].Value = formDataa["countrycode"];
                        cmd.Parameters.Add(new SqlParameter("@address", SqlDbType.NVarChar));
                        cmd.Parameters["@address"].Value = formDataa["address"];
                        cmd.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                        cmd.Parameters["@RoleId"].Value = 4;
                        cmd.Parameters.Add(new SqlParameter("@noOfToysDonated", SqlDbType.NVarChar));
                        cmd.Parameters["@noOfToysDonated"].Value = formDataa["noOfToys"];
                        cmd.Parameters.Add(new SqlParameter("@TrackingIdD", SqlDbType.NVarChar));
                        cmd.Parameters["@TrackingIdD"].Value = trackingid;
                        cmd.Parameters.Add(new SqlParameter("@status", SqlDbType.NVarChar));
                        cmd.Parameters["@status"].Value = "Waiting for collection";
                        cmd.Parameters.Add(new SqlParameter("@dateOfDonation", SqlDbType.NVarChar));
                        cmd.Parameters["@dateOfDonation"].Value = DateTime.Now.Date;
                        cmd.Parameters.Add(new SqlParameter("@TrackingNumber", SqlDbType.NVarChar));
                        cmd.Parameters["@TrackingNumber"].Value = trackingNumber;
                        cmd.Parameters.Add(new SqlParameter("@images", SqlDbType.Structured));
                        cmd.Parameters["@images"].Value = logColl;
                        cmd.Connection = connection;
                        connection.Open();
                        string message = Convert.ToString(cmd.ExecuteScalar());
                        connection.Close();
                        if (message == "saved")
                        {
                            //return true;

                        }
                        else //error
                        {
                            //return false;
                        }
                    }
                    string uemail = formDataa["email"];
                    string ufirstname = formDataa["fullName"];
                    //For to send an email at Gmail Starts: gmail ID: setting link: https://myaccount.google.com/lesssecureapps?pli=1
                    var fromAddress = new MailAddress("toys4smile@gmail.com", "Toys4Smile");
                    var toAddress = new MailAddress(uemail, "Toys4Smile");
                    const string fromPassword = "Toys1234";
                    string subject = "Donation Successful - Tracking ID: " + trackingid;
                    string body = "Dear " + ufirstname + ", \r\n \r\n" +
                        "Your donation request has been received. \r\nTo track your donation your Tracking Id is: "
                        + trackingid + "\r\n \r\nThank you for your donation. \r\n \r\n Regards,\r\n Toys4Smile Team";
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                        Timeout = 20000
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(message);
                    }
                    //For to send an email at Gmail Ends
                }
                return trackingid;
            }
            catch (Exception ex)
            {
                return trackingid;
            }
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/donateAsRegisteredUser")]
        public string donateAsRegisteredUser()
        {
            string useremail = "";
            string userfirstname = "";
            string trackingid = "";
            string trackingNumber = "";
            try
            {
                var formFiles = HttpContext.Current.Request.Files;
                var formDataa = HttpContext.Current.Request.Form;
                Toy4SmileEntities t = new Toy4SmileEntities();
                DonationViewModel dv = new DonationViewModel();
                var ss = DateTime.Now;
                string year = ss.Year.ToString();
                string month = ss.Month.ToString();
                string day = ss.Day.ToString();
                //string connectionString = "Data Source=DESKTOP-V4VKSII\\SQLEXPRESS;Initial Catalog=Toy4Smile;Integrated Security=true";
                string connectionString = "data source=184.154.206.133; initial catalog=Toy4Smile;user id=toyssmil; password=98YEvge67s";
                using (SqlConnection connection =
                    new SqlConnection(connectionString))
                {
                    // Create the Command and Parameter objects.
                    SqlCommand command = new SqlCommand("sp_trackingNumber", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@date", SqlDbType.Date));
                    command.Parameters["@date"].Value = DateTime.Now;
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        var comlumExist = HasColumn(reader, "DonationId");
                        if (comlumExist == true)
                        {
                            while (reader.Read())
                            {
                                if (reader != null)
                                {
                                    var sss = reader["DonationId"];
                                    var trackingNumbers = Convert.ToInt32(reader["TrackingNumber"]);
                                    trackingNumbers = trackingNumbers + 1;
                                    trackingNumber = trackingNumbers.ToString();
                                }
                            }
                            var length = trackingNumber.Length;
                            if (length == 1)
                            {
                                trackingNumber.Insert(0, "000");
                                trackingNumber = "000" + trackingNumber;
                            }
                            else if (length == 2)
                            {
                                trackingNumber.Insert(0, "00");
                                trackingNumber = "00" + trackingNumber;
                            }
                            else if (length == 3)
                            {
                                trackingNumber.Insert(0, "0");
                                trackingNumber = "0" + trackingNumber;
                            }
                            trackingid = year + "-" + month + "-" + day + "-" + trackingNumber;
                        }
                        else
                        {
                            trackingid = year + "-" + month + "-" + day + "-" + "0001";
                            trackingNumber = "0001";
                        }
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    Donation d = new Donation();
                    d.noOfToysDonated = formDataa["noOfToys"];
                    d.SpecialComments = formDataa["specialComments"];
                    d.OrphanageId = Convert.ToInt32(formDataa["selectedOrphanageId"]);
                    d.UserId = Convert.ToInt32(formDataa["userId"]);
                    useremail = formDataa["useremail"];
                    userfirstname = formDataa["userfirstname"];
                    d.TrackingId = trackingid;
                    d.status = "Waiting for collection";
                    d.dateOfDonation = DateTime.Now.Date;
                    d.TrackingNumber = trackingNumber;
                    var Rd = t.Donations.Add(d);
                    t.SaveChanges();
                    List<string> files = new List<string>();
                    try
                    {
                        foreach (string item in formFiles)
                        {
                            HttpPostedFile file = formFiles[item];
                            if (file.ContentLength > 0)
                            {
                                var dataUrl = file.FileName;
                                image i = new image();
                                i.DonationId = Rd.DonationId;
                                i.imageName = dataUrl;
                                t.images.Add(i);
                                t.SaveChanges();
                            }
                        }
                    }
                    catch (Exception)
                    {
                        return trackingid = "";
                    }
                    //For to send an email at Gmail Starts: gmail ID: setting link: https://myaccount.google.com/lesssecureapps?pli=1
                    var fromAddress = new MailAddress("toys4smile@gmail.com", "Toys4Smile");
                    var toAddress = new MailAddress(useremail, "Toys4Smile");
                    const string fromPassword = "Toys1234";
                    string subject = "Donation Successful - Tracking ID: " + trackingid;
                    string body = "Dear " + userfirstname + ", \r\n \r\n" +
                        "Your donation request has been received. \r\nTo track your donation your Tracking Id is : "
                        + trackingid + "\r\n \r\nThank you for your donation. \r\n \r\n Regards,\r\n Toys4Smile Team";
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                        Timeout = 20000
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(message);
                    }
                    //For to send an email at Gmail Ends
                    return trackingid;
                }
            }
            catch (Exception ex)
            {
                return trackingid;
            }
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/donateAsAdmin")]
        public bool donateAsAdmin()
        {
            try
            {
                string trackingid = "";
                string trackingNumber = "";
                var formFiles = HttpContext.Current.Request.Files;
                var formDataa = HttpContext.Current.Request.Form;
                Toy4SmileEntities t = new Toy4SmileEntities();
                DonationViewModel dv = new DonationViewModel();
                var ss = DateTime.Now;
                string year = ss.Year.ToString();
                string month = ss.Month.ToString();
                string day = ss.Day.ToString();
                //string connectionString = "Data Source=DESKTOP-V4VKSII\\SQLEXPRESS;Initial Catalog=Toy4Smile;Integrated Security=true";
                string connectionString = "data source=184.154.206.133; initial catalog=Toy4Smile;user id=toyssmil; password=98YEvge67s";
                using (SqlConnection connection =
                    new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand("sp_trackingNumber", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@date", SqlDbType.Date));
                    command.Parameters["@date"].Value = DateTime.Now;
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        var comlumExist = HasColumn(reader, "DonationId");
                        if (comlumExist == true)
                        {
                            while (reader.Read())
                            {
                                if (reader != null)
                                {
                                    var sss = reader["DonationId"];
                                    var trackingNumbers = Convert.ToInt32(reader["TrackingNumber"]);
                                    trackingNumbers = trackingNumbers + 1;
                                    trackingNumber = trackingNumbers.ToString();
                                }
                            }
                            var length = trackingNumber.Length;
                            if (length == 1)
                            {
                                trackingNumber.Insert(0, "000");
                                trackingNumber = "000" + trackingNumber;
                            }
                            else if (length == 2)
                            {
                                trackingNumber.Insert(0, "00");
                                trackingNumber = "00" + trackingNumber;
                            }
                            else if (length == 3)
                            {
                                trackingNumber.Insert(0, "0");
                                trackingNumber = "0" + trackingNumber;
                            }
                            trackingid = year + "-" + month + "-" + day + "-" + trackingNumber;
                        }
                        else
                        {
                            trackingid = year + "-" + month + "-" + day + "-" + "0001";
                            trackingNumber = "0001";
                        }
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    User u = new User();
                    u.firstName = formDataa["fullName"];
                    u.mobile = formDataa["mobile"];
                    u.email = formDataa["email"];
                    u.countrycode = formDataa["countrycode"];
                    u.address = formDataa["address"];
                    u.RoleId = 4;
                    var s = t.Users.Add(u);
                    t.SaveChanges();
                    Donation d = new Donation();
                    d.noOfToysDonated = formDataa["noOfToys"];
                    d.UserId = s.UserId;
                    d.TrackingId = trackingid;
                    d.status = "Waiting for collection";
                    d.dateOfDonation = DateTime.Now.Date;
                    d.TrackingNumber = trackingNumber;
                    d.SpecialComments = formDataa["specialComments"];
                    d.OrphanageId = Convert.ToInt32(formDataa["selectedOrphanageId"]);
                    var Rd = t.Donations.Add(d);
                    t.SaveChanges();
                    List<string> files = new List<string>();
                    try
                    {
                        foreach (string item in formFiles)
                        {
                            HttpPostedFile file = formFiles[item];
                            if (file.ContentLength > 0)
                            {
                                var dataUrl = file.FileName;
                                image i = new image();
                                i.DonationId = Rd.DonationId;
                                i.imageName = dataUrl;
                                t.images.Add(i);
                                t.SaveChanges();
                            }
                        }
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/getAllUsersByRoleId/{roleId}")]
        public List<User> getAllUsers(int roleId)
        {
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                return (from u in dal.Users where u.RoleId == roleId select u).ToList();
            }
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/deleteUser")]
        public bool deleteUser(User value)
        {
            try
            {
                Toy4SmileEntities t = new Toy4SmileEntities();
                var dd = t.Users.First(x => x.UserId == value.UserId);
                t.Users.Remove(dd);
                t.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/registerUserAsAdmin")]
        public bool registerUserAsAdmin(UserViewModel u)
        {
            try
            {
                Toy4SmileEntities t = new Toy4SmileEntities();
                User user = new User();
                user.firstName = u.firstName;
                user.lastName = u.lastName;
                user.mobile = u.mobile;
                user.email = u.email;
                user.dob = Convert.ToDateTime(u.dob);
                user.password = u.password;
                user.address = u.address;
                user.status = true;
                user.OrphanageTitle = u.ngoName;
                user.countrycode = u.countrycode;
                if (u.RoleName == "Admin")
                {
                    user.RoleId = 2;
                }
                else if (u.RoleName == "Orphanage")
                {
                    user.RoleId = 5;
                }
                else if (u.RoleName == "Donor")
                {
                    user.RoleId = 3;
                }
                user.date = DateTime.Now;
                t.Users.Add(user);
                t.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/getUserById/{userId}")]
        public UserViewModel getUserById(int userId)
        {
            UserViewModel d = new UserViewModel();
            Toy4SmileEntities t = new Toy4SmileEntities();
            var ff = (from s in t.Users
                      join sa in t.Roles on s.RoleId equals sa.RoleId
                      where s.UserId == userId
                      select s);
            var f = (from p in t.Users
                     join e in t.Roles
                     on p.RoleId equals e.RoleId
                     where p.UserId == userId
                     select new
                     {
                         ID = p.UserId,
                         address = p.address,
                         date = p.date,
                         lastName = p.lastName,
                         RoleId = e.RoleId,
                         RoleName = e.RoleName,
                         dob = p.dob,
                         email = p.email,
                         firstName = p.firstName,
                         mobile = p.mobile,
                         password = p.password,
                         status = p.status,
                         countrycode = p.countrycode,
                         orphanagetitle = p.OrphanageTitle,
                     }).FirstOrDefault();
            d.date = Convert.ToDateTime(f.date);
            d.address = f.address;
            d.dob = f.dob;
            d.email = f.email;
            d.firstName = f.firstName;
            d.RoleId = Convert.ToInt32(f.RoleId);
            d.RoleName = f.RoleName;
            d.lastName = f.lastName;
            d.mobile = f.mobile;
            d.password = f.password;
            d.UserId = f.ID;
            d.status = Convert.ToBoolean(f.status);
            d.ngoName = f.orphanagetitle;
            d.countrycode = f.countrycode;
            return d;
        }

        [HttpGet]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/IsAuthorized")]
        [Authorize]
        public bool IsAuthorized()
        {
            return true;
        }

        public static bool HasColumn(DbDataReader Reader, string ColumnName)
        {
            foreach (DataRow row in Reader.GetSchemaTable().Rows)
            {
                if (row["ColumnName"].ToString() == ColumnName)
                    return true;
            }
            return false;
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/editUserAsAdmin")]
        public bool editUserAsAdmin(UserViewModel u)
        {
            try
            {
                Toy4SmileEntities t = new Toy4SmileEntities();
                var user = t.Users.First(x => x.UserId == u.UserId);
                t.SaveChanges();
                user.firstName = u.firstName;
                user.lastName = u.lastName;
                user.mobile = u.mobile;
                user.email = u.email;
                user.dob = Convert.ToDateTime(u.dob);
                user.password = u.password;
                user.address = u.address;
                user.OrphanageTitle = u.ngoName;
                user.countrycode = u.countrycode;
                if (u.RoleName == "Admin")
                {
                    user.RoleId = 2;
                }
                else if (u.RoleName == "Orphanage")
                {
                    user.RoleId = 5;
                }
                else if (u.RoleName == "Donor")
                {
                    user.RoleId = 3;
                }
                t.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/editDonationAsAdmin")]
        public bool editDonationAsAdmin()
        {
            try
            {
                string trackingid = "";
                string trackingNumber = "";
                var formFiles = HttpContext.Current.Request.Files;
                var formDataa = HttpContext.Current.Request.Form;
                Toy4SmileEntities t = new Toy4SmileEntities();
                DonationViewModel dv = new DonationViewModel();
                User u = new User();
                int userId = Convert.ToInt32(formDataa["userId"]);
                var editUser = t.Users.First(x => x.UserId == userId);
                editUser.mobile = formDataa["mobile"];
                editUser.address = formDataa["address"];
                editUser.countrycode = formDataa["countrycode"];
                t.SaveChanges();
                int donationId = Convert.ToInt32(formDataa["donationId"]);
                var editDonation = t.Donations.First(x => x.DonationId == donationId);
                editDonation.noOfToysDonated = formDataa["noOfToys"];
                editDonation.SpecialComments = formDataa["specialComments"];
                editDonation.OrphanageId = Convert.ToInt32(formDataa["selectedOrphanageId"]);
                t.SaveChanges();
                List<string> files = new List<string>();
                try
                {
                    foreach (string item in formFiles)
                    {
                        HttpPostedFile file = formFiles[item];
                        if (file.ContentLength > 0)
                        {
                            var dataUrl = file.FileName;
                            image i = new image();
                            i.DonationId = donationId;
                            i.imageName = dataUrl;
                            t.images.Add(i);
                            t.SaveChanges();
                        }
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/Donor/editDonationAsOrphanage")]
        public bool editDonationAsOrphanage()
        {
            try
            {
                string trackingid = "";
                string trackingNumber = "";
                var formFiles = HttpContext.Current.Request.Files;
                var formDataa = HttpContext.Current.Request.Form;
                Toy4SmileEntities t = new Toy4SmileEntities();
                DonationViewModel dv = new DonationViewModel();
                int donationId = Convert.ToInt32(formDataa["donationId"]);
                var editDonation = t.Donations.First(x => x.DonationId == donationId);
                editDonation.noOfToysDonated = formDataa["noOfToys"];
                t.SaveChanges();
                List<string> files = new List<string>();
                try
                {
                    foreach (string item in formFiles)
                    {
                        HttpPostedFile file = formFiles[item];
                        if (file.ContentLength > 0)
                        {
                            var dataUrl = file.FileName;
                            image i = new image();
                            i.DonationId = donationId;
                            i.imageName = dataUrl;
                            t.images.Add(i);
                            t.SaveChanges();
                        }
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

    public class gridClass
    {
        public string name { get; set; }
    }

    public class logCollection : List<gridClass>, IEnumerable<SqlDataRecord>
    {
        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            var sqlRow = new SqlDataRecord(
            new SqlMetaData("name", SqlDbType.VarChar, SqlMetaData.Max));
            foreach (gridClass log in this)
            {
                sqlRow.SetString(0, log.name);
                yield return sqlRow;
            }
        }
    }
}
