﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Toy4Smile.Models.ViewModels;

namespace Toy4Smile.Controllers
{
    public class OrphanageController : ApiController
    {
        [HttpPost]
        [Route("api/Orphanage/ChangePassword")]
        public int ChangePassword(ChangePasswordViewModel passwords)
        {
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                User user = (from u in dal.Users where u.UserId == passwords.userId && u.RoleId == passwords.roleId select u).FirstOrDefault();
                if (user != null)
                {
                    if (user.password != passwords.oldPassword)
                    {
                        return 0;
                    }
                    else
                    {
                        user.password = passwords.newPassword;
                        dal.SaveChanges();
                        return 1;
                    }
                }
                else
                {
                    return 2;
                }
            }
        }

        [HttpGet]
        [Route("api/Orphanage/GetOrphanageTitle")]
        public List<OrphanageTitleViewModel> GetOrphanageTitle()
        {
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                List<User> users = (from u in dal.Users where u.RoleId == 5 select u).ToList();
                List<OrphanageTitleViewModel> TempList = null;
                if (users != null)
                {
                    TempList = new List<OrphanageTitleViewModel>();
                    foreach (var u in users)
                    {
                        OrphanageTitleViewModel otm = new OrphanageTitleViewModel();
                        otm.OrphanageId = u.UserId;
                        otm.OrphanageTitle = u.OrphanageTitle;
                        TempList.Add(otm);
                    }
                    TempList.TrimExcess();
                    return TempList;
                }
                else
                {
                    return TempList;
                }
            }
        }
    }
}
