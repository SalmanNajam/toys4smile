﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Toy4Smile.Models.ViewModels;

namespace Toy4Smile.Controllers
{
    public class AdminController : ApiController
    {
        [HttpPost]
        [Route("api/Admin/ChangeUserStatus")]
        public bool ChangeUserStatus(UserViewModel userModel)
        {
            try
            {
                using (Toy4SmileEntities dal = new Toy4SmileEntities())
                {
                    User user = (from u in dal.Users where u.UserId == userModel.UserId && u.RoleId == userModel.RoleId select u).FirstOrDefault();
                    if (user != null)
                    {
                        user.status = userModel.status;
                        dal.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
