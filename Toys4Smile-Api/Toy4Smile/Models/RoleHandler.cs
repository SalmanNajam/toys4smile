﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Toy4Smile.Models
{
    public class RoleHandler
    {
        public static List<Role> GetRoles()
        {
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                return (from roles in dal.Roles select roles).ToList();
            }
        }

        public static Role GetRoleById(int id)
        {
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                return (from role in dal.Roles where role.RoleId == id select role).FirstOrDefault();   
            }
        }

        public static Boolean DeleteRole(int id)
        {
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                try
                {
                    Role toDelete = (from role in dal.Roles where role.RoleId == id select role).FirstOrDefault();
                    dal.Roles.Remove(toDelete);
                    dal.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {

                    return false;
                }
            }
        }

        public static Boolean AddRole(Role role)
        {
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                try
                {
                    dal.Roles.Add(role);
                    dal.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static Boolean EditRole(int id, Role role)
        {
            using (Toy4SmileEntities dal = new Toy4SmileEntities())
            {
                try
                {
                    Role toEdit = (from r in dal.Roles where role.RoleId == id select r).FirstOrDefault();
                    toEdit.RoleName = role.RoleName;
                    dal.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
        }
    }
}