﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Toy4Smile.Models.ViewModels
{
    public class OrphanageTitleViewModel
    {
        public int OrphanageId { get; set; }
        public string OrphanageTitle { get; set; }
    }
}