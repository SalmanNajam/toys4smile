﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Toy4Smile.Models.ViewModels
{
    public class DonationViewModel
    {
        //public int DonationId { get; set; }
        //public int TrackingId { get; set; }
        //public int UserId { get; set; }
        //public string status { get; set; }
        //public DateTime dateOfDonation { get; set; }
        //public string trackingNo { get; set; }
        //public string email { get; set; }
        //public string mobile { get; set; }
        //public string noOfToysDonated { get; set; }
        //public int imageId { get; set; }
        //public DateTime date { get; set; }
        //public string password { get; set; }
        //public string firstName { get; set; }
        //public string lastName { get; set; }
        //public DateTime dob { get; set; }
        //public string address { get; set; }
        //public string imageName { get; set; }
        //public int DonationId1 { get; set; }
        public int DonationId { get; set; }
        public int TrackingId { get; set; }
        public int UserId { get; set; }
        public string status { get; set; }
        public DateTime dateOfDonation { get; set; }
        public int ImageId { get; set; }
        public int DonationId1 { get; set; }
        public string imageName { get; set; }
        public int TrackingId1 { get; set; }
        public string trackingNo { get; set; }
        public int UserId1 { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string noOfToysDonated { get; set; }
        public int imageId1 { get; set; }
        public DateTime date { get; set; }
        public string password { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime dob { get; set; }
        public string address { get; set; }

    }
}