﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Toy4Smile.Models.ViewModels
{
    public class RegisterUserModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string mobileNo { get; set; }
        public string dob { get; set; }
        public string password { get; set; }
        public string address { get; set; }
        public string countrycode { get; set; }
    }
}