﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Toy4Smile.Models.ViewModels
{
    public class ChangePasswordViewModel
    {
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public int userId { get; set; }
        public int roleId { get; set; }
    }
}