﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Toy4Smile.Models.ViewModels
{
    public class UserViewModel
    {
        public int UserId;
        public string email { get; set; }
        public string mobile { get; set; }
        public string noOfToysDonated { get; set; }
        public Nullable<int> imageId { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string password { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public Nullable<System.DateTime> dob { get; set; }
        public string address { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool status { get; set; }
        public string ngoName { get; set; }
        public string countrycode { get; set; }
    }
}