﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Toy4Smile.Models.ViewModels
{
    public class donationByIdViewModel
    {
        public int DonationId { get; set; }
        public string TrackingId { get; set; }
        public int UserId { get; set; }
        public string status { get; set; }
        public DateTime dateOfDonation { get; set; }
        public string noOfToysDonated { get; set; }
        public string TrackingNumber { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string address { get; set; }
        public string mobile { get; set; }
        public string countrycode { get; set; }
        public string specialComments { get; set; }
        public int? orphanageId { get; set; }
        public List<imagePathss> imagePathss = new List<imagePathss>();
    }
}