﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Toy4Smile.Models;
using Toy4Smile.Models.ViewModels;

using Microsoft.Owin;

// Installing Command of CORS: Install-Package Microsoft.AspNet.WebApi.Cors -Version 5.2.6

namespace Toy4Smile
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            using (var db = new Toy4SmileEntities())
            {
                if (db != null)
                {
                    User user = db.Users.Where(u => u.email == context.UserName && u.password == context.Password).FirstOrDefault();
                    if (user != null)
                    {
                        Role userRole = db.Roles.Where(r => r.RoleId == user.RoleId).FirstOrDefault();
                        
                        {
                            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                            identity.AddClaim(new Claim("Userid", user.UserId.ToString()));
                            if (user.firstName != "" || user.firstName != null)
                            {
                                identity.AddClaim(new Claim("Firstname", user.firstName));
                            }
                            //if (user.lastName != "" && user.lastName != null)
                            //{
                            //    identity.AddClaim(new Claim("Lastname", user.lastName));
                            //}
                            if (user.email != "" && user.email != null)
                            {
                                identity.AddClaim(new Claim("Email", user.email));
                            }
                            if (user.mobile != "" && user.mobile != null)
                            {
                                identity.AddClaim(new Claim("Mobile", user.mobile));
                            }
                            if (user.address != "" && user.address != null)
                            {
                                identity.AddClaim(new Claim("Address", user.address));
                            }
                            if (user.status != null)
                            {
                                identity.AddClaim(new Claim("Status", user.status.ToString()));
                            }
                            //if (user.date.ToString() != "" && user.date.ToString() != null)
                            //{
                            //    identity.AddClaim(new Claim("CreatedDate", user.date.ToString()));
                            //}
                            //if (user.dob.ToString() != "" && user.dob.ToString() != null)
                            //{
                            //    identity.AddClaim(new Claim("Dob", user.dob.ToString()));
                            //} else
                            //{
                            //    identity.AddClaim(new Claim("Dob", Convert.ToString(0000 - 00 - 00)));
                            //}
                            if (user.RoleId.ToString() != "" && user.RoleId.ToString() != null)
                            {
                                identity.AddClaim(new Claim("Roleid", user.RoleId.ToString()));
                            }
                            if (userRole.RoleName != "" && userRole.RoleName != null)
                            {
                                identity.AddClaim(new Claim("RoleName", userRole.RoleName));
                            }
                            context.Validated(identity);
                        }
                    }
                    else
                    {
                        context.SetError("invalid_grant", "Provided username and password is incorrect");
                        context.Rejected();
                    }
                    return;
                }
            }
        }
    }
}