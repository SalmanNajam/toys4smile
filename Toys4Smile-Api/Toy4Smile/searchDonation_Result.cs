//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Toy4Smile
{
    using System;
    
    public partial class searchDonation_Result
    {
        public int DonationId { get; set; }
        public string TrackingId { get; set; }
        public Nullable<int> UserId { get; set; }
        public string status { get; set; }
        public Nullable<System.DateTime> dateOfDonation { get; set; }
        public string noOfToysDonated { get; set; }
        public string TrackingNumber { get; set; }
        public string SpecialComments { get; set; }
        public Nullable<int> OrphanageId { get; set; }
        public int TrackingId1 { get; set; }
        public string trackingNo { get; set; }
        public int UserId1 { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string countrycode { get; set; }
        public Nullable<int> RoleId { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string password { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public Nullable<System.DateTime> dob { get; set; }
        public string address { get; set; }
        public string OrphanageTitle { get; set; }
        public Nullable<bool> status1 { get; set; }
    }
}
