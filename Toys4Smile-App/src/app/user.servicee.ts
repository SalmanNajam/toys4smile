import { Injectable } from '@angular/core';
import { registerUser } from './Models/registerUser';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { donationViewModel } from './models/donationViewModel';
import { adminUserModel } from './models/adminUserModel';
import { Email } from './models/email.model';

@Injectable()
export class UserServicee {
    constructor(private http: HttpClient) { }

    registerUser(user: registerUser): Observable<boolean> {
        return this.http.post<boolean>('http://api.toys4smile.com/api/Donor/registerUser', user);
    }

    registerUserAsAdmin(user: adminUserModel): Observable<boolean> {
        return this.http.post<boolean>('http://api.toys4smile.com/api/Donor/registerUserAsAdmin', user);
    }

    donateAsGuest(formddata: FormData): Observable<string> {
        return this.http.post<string>('http://api.toys4smile.com/api/Donor/donateAsGuest', formddata);
    }

    donateAsRegisteredUser(formddata: FormData): Observable<string> {
        return this.http.post<string>('http://api.toys4smile.com/api/Donor/donateAsRegisteredUser', formddata);
    }

    donateAsAdmin(formddata: FormData) {
        return this.http.post('http://api.toys4smile.com/api/Donor/donateAsAdmin', formddata);
    }
    searchDonationByTrackingId(trackingId: string): Observable<donationViewModel> {

        return this.http.get<donationViewModel>('http://api.toys4smile.com/api/Donations/getAllDonationsByTrackingId/' + trackingId);
    }

    hasEmailRegistered(email: Email): Observable<boolean> {
        return this.http.post<boolean>('http://api.toys4smile.com/api/Donor/HasEmailRegistered', email);
    }

    editUserAsAdmin(user: adminUserModel): Observable<boolean> {
        return this.http.post<boolean>('http://api.toys4smile.com/api/Donor/editUserAsAdmin', user);
    }

    editDonationAsAdmin(formddata: FormData): Observable<boolean>  {
        return this.http.post<boolean>('http://api.toys4smile.com/api/Donor/editDonationAsAdmin', formddata);
    }

    editDonationAsOrphanage(formddata: FormData): Observable<boolean>  {
        return this.http.post<boolean>('http://api.toys4smile.com/api/Donor/editDonationAsOrphanage', formddata);
    }

    errorHandler(error: Response) {
        console.log(error);
        return Observable.throw(error);
    }

}
