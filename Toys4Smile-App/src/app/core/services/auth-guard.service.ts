import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User } from '../../models/user.model';

@Injectable()
export class AuthGuard implements CanActivate {
    isAuthorised: boolean;
    constructor(private router: Router) { }
    user: User;
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.user = null;
        this.user = JSON.parse(localStorage.getItem('user'));
        if (localStorage.getItem('access_token')) {
            if (this.user !== null && this.user.status === true) {
                return true;
            }
        }
        if (this.user !== null && this.user.status === false) {
            this.router.navigate(['disabled-user'], { queryParams: { returnUrl: state.url } });
            return false;
        }
        this.router.navigate(['signin'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
