import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';

@Injectable()
export class AdminGuard implements CanActivate {
    isAuthorised: boolean;
    user: User;
    constructor(private router: Router, private userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.user = JSON.parse(localStorage.getItem('user'));
        if (this.user != null || this.user != undefined) {
            if (this.user.RoleId === 1 || this.user.RoleId === 2) {
                return true;
            } else {
                return false;
            }
        }

        // not logged in so redirect to login page with the return url and return false
        this.router.navigate(['signin'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
