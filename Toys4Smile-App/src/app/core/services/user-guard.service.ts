import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { User } from '../../models/user.model';

@Injectable()
export class UserGuard implements CanActivate {
    isAuthorised: boolean;
    user: User;
    constructor(private router: Router) {
    }

    canActivate() {
        this.user = null;
        this.user = JSON.parse(localStorage.getItem('user'));
        console.log(this.user);
        if (this.user !== null && this.user.status === false) {
            this.user = null;
            return true;
        }
        this.user = null;
        this.router.navigate(['disabled-user']);
        return false;
    }
}