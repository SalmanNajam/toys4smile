import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
declare var jQuery: any;

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {
    user: User;
    isLoggedIn: boolean;
    url: string;

    constructor(private userService: UserService) {
            this.user = JSON.parse(localStorage.getItem('user'));
            let token = localStorage.getItem('access_token');
            if (token != null) {
                this.isLoggedIn = true;
            } else {
                this.isLoggedIn = false;
            }
        setTimeout(() => {
            this.userService.isAuthorized().subscribe(
                (response) => {
                    if (response === true) {
                        this.user = JSON.parse(localStorage.getItem('user'));
                        this.isLoggedIn = true;
                    } else {
                        this.isLoggedIn = false;
                        localStorage.removeItem('access_token');
                        localStorage.removeItem('expires');
                        localStorage.removeItem('user');
                    }
                },
                (errors) => {
                    this.isLoggedIn = false;
                    localStorage.removeItem('access_token');
                    localStorage.removeItem('expires');
                    localStorage.removeItem('user');
                });
            (function ($) {
                "use strict";
                var $body = $('body'),
                    $wrapper = $('.body-innerwrapper'), 
                    $toggler = $('#offcanvas-toggler'),
                    $close = $('.close-offcanvas'),
                    $offCanvas = $('.offcanvas-menu');
                $toggler.on('click', function (event) {
                    event.preventDefault();
                    stopBubble(event);
                    setTimeout(offCanvasShow, 50);
                });
                $close.on('click', function (event) {
                    event.preventDefault();
                    offCanvasClose();
                });
                var offCanvasShow = function () {
                    $body.addClass('offcanvas');
                    $wrapper.on('click', offCanvasClose);
                    $close.on('click', offCanvasClose);
                    $offCanvas.on('click', stopBubble);
                };
                var offCanvasClose = function () {
                    $body.removeClass('offcanvas');
                    $wrapper.off('click', offCanvasClose);
                    $close.off('click', offCanvasClose);
                    $offCanvas.off('click', stopBubble);
                };
                var stopBubble = function (e) {
                    e.stopPropagation();
                    return true;
                };
                offCanvasClose();
            })(jQuery);
        }, 0);
    }

    ngOnInit() {
    }

}
