import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AboutusComponent } from "./aboutus.component";
import { SharedModule } from "../shared/shared.module";
import { AboutUsRoutingModule } from "./aboutus-routing.module";

@NgModule({
  imports: [CommonModule, AboutUsRoutingModule, SharedModule],
  declarations: [AboutusComponent]
})

export class AboutUsModule {}