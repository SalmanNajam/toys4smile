import { imagePaths } from './imagePaths';
export class donationByIdViewModel {
    DonationId: number;
    TrackingId: string;
    UserId: number;
    status: string;
    dateOfDonation: Date;
    TrackingNumber: string;
    noOfToysDonated: string;
    email: string;
    firstName: string;
    lastName: string;
    imagePathss: imagePaths[];
    address: string;
    mobile: string;
    countrycode: string;
    orphanageId: number;
    specialComments: string;
}