export interface Donation {
    DonationId: number;
    TrackingId: string;
    UserId: number;
    status: string;
    dateOfDonation: Date
}