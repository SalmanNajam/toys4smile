export class adminUserModel {
    UserId: number;
    firstName: string;
    lastName: string;
    email: string;
    mobile: string;
    address: string;
    RoleId: number;
    RoleName: string;
    date: string;
    dob: string;
    password: string;
    ngoName: string;
    countrycode: string;
}