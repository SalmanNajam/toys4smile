export interface User {
    UserId: number;
    firstName: string;
    lastName: string;
    email: string;
    mobile: string;
    address: string;
    RoleId: number;
    RoleName: string;
    date: Date;
    dob: Date;
    status: boolean;
}