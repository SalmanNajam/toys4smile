export class registerUser {
    firstName: string;
    lastName: string;
    email: string;
    mobileNo: string;
    dob: string;
    password: string;
    address: string;
    countrycode: string;
}