export class recentDonationViewModel{
    DonationId: number;
    TrackingId: string;
    UserId: number;
    status: string;
    dateOfDonation: Date;
    TrackingNumber: string;
    noOfToysDonated:string;
    SpecialComments:string;
    imageName:string;
}