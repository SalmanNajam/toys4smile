export class adminEditDonationsViewModel {
    mobile: string;
    noOfToys: string;
    email: string;
    address: string;
    countrycode: string;
    specialComments: string;
    orphanageId: number;
    trackingid: string;
}