export interface OrphanageTitles {
    OrphanageId: number;
    OrphanageTitle: string;
}