export class ChangePassword {
    oldPassword: string;
    newPassword: string;
    userId: number;
    roleId: number;
}