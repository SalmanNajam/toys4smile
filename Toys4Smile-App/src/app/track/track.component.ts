import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { donationViewModel } from '../models/donationViewModel';
import { DonationService } from '../donation.service';
import { User } from '../models/user.model';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var jQuery: any;

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html'
})

export class TrackComponent implements OnInit {
  loading: boolean;
  isLoggedIn: boolean;
  dateString: string = '2018-07-07';
  newDate: Date = new Date(this.dateString);
  trackResult: donationViewModel;
  user: User;
  searchDonation: FormGroup;

  constructor(private fb: FormBuilder, private donationService: DonationService,
    private spinner: NgxSpinnerService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    this.createForm();
    this.user = JSON.parse(localStorage.getItem('user'));
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);
        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
  }

  createForm() {
    this.searchDonation = this.fb.group({
      trackingId: ['', Validators.required]
    })
  }

  ngOnInit() { }

  searchDonations() {
    this.spinner.show();
    const formModel = this.searchDonation.value;
    var searchTrackingId = formModel.trackingId as string;
    this.donationService.getDonationByTrackingId(searchTrackingId).subscribe(
      (response) => {
        this.spinner.hide();
        if (response != null) {
          this.toastr.success('One record found!');
          this.trackResult = response;
        } else {
          this.spinner.hide();
          this.toastr.success('No record found, against provided Tracking ID!');
        }
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.success('No record found, against provided Tracking ID!');
      }
    );
  }
}
