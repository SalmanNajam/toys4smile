import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { TrackRoutingModule } from "./track-routing.module";
import { TrackComponent } from "./track.component";

@NgModule({
  imports: [CommonModule, TrackRoutingModule, SharedModule],
  declarations: [TrackComponent]
})

export class TrackModule { }