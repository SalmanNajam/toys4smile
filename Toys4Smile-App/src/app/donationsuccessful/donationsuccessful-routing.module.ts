import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DonationsuccessfulComponent } from "./donationsuccessful.component";

const routes: Routes = [
    { path: "", component: DonationsuccessfulComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class DonationsuccessfulRoutingModule { }