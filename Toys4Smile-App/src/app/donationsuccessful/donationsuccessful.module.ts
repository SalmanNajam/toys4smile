import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { DonationsuccessfulRoutingModule } from "./donationsuccessful-routing.module";
import { DonationsuccessfulComponent } from "./donationsuccessful.component";

@NgModule({
  imports: [CommonModule, DonationsuccessfulRoutingModule, SharedModule],
  declarations: [DonationsuccessfulComponent]
})

export class DonationSuccessfulModule {}