import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { SignInRoutingModule } from "./signin-routing.module";
import { SigninComponent } from "./signin.component";

@NgModule({
    imports: [CommonModule, SignInRoutingModule, SharedModule],
    declarations: [SigninComponent]
})

export class SignInModule { }