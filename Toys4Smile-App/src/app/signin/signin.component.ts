import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var jQuery: any;

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html'
})

export class SigninComponent implements OnInit {
  user: User;
  form: FormGroup;
  isLoggedIn: boolean;
  returnUrlDonor: string;
  returnUrlAdmin: string;
  returnUrl: string;

  constructor(private userService: UserService, private router: Router,
    private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef,
    private spinner: NgxSpinnerService) {
    this.toastr.setRootViewContainerRef(vcr);
    this.user = JSON.parse(localStorage.getItem('user'));
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);
        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
  }

  ngOnInit() {
    // get return url from route parameters or default to '/'
    this.returnUrlDonor = this.route.snapshot.queryParams['returnUrlDonor'] || '/donor';
    this.returnUrlAdmin = this.route.snapshot.queryParams['returnUrlAdmin'] || '/admin-donations';
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    })
  }

  onSignIn() {
    this.spinner.show();
    this.userService.onSignIn(this.form.value.email, this.form.value.password).subscribe(
      (response) => {
        localStorage.setItem('access_token', response.access_token);
        localStorage.setItem('expires', response.expires_in);
        this.userService.getDonorDetails().subscribe(
          (response) => {
            this.user = response;
            localStorage.setItem('user', JSON.stringify(this.user));
            this.user = JSON.parse(localStorage.getItem('user'));
            if (this.user.status === false) {
              this.router.navigate(['disabled-user']);
            } else if (this.user.RoleId === 1 || this.user.RoleId === 2) {
              this.router.navigate(['admin/admin-donations']);
            } else if (this.user.RoleId === 3) {
              this.router.navigate(['donor']);
            } else if (this.user.RoleId === 5) {
              this.router.navigate(['orphanage/orphanage-dashboard']);
            } else {
              this.router.navigate(['/']);
            }
            this.spinner.hide();
          },
          (errors) => {
            this.spinner.hide();
            this.toastr.error('Network Error/Internet connectivity issue... Please try later!');
          }
        );
      },
      (errors) => {
        this.spinner.hide();
        if (errors.status === 400) {
          this.toastr.error('Invalid Email or Password!');
        } else {
          this.toastr.error('Network Error/Internet connectivity issue... Please try later!');
        }
      }
    );
  }
}
