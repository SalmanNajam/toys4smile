import { NgModule }  from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from '../HeaderFooter/header/header.component';
import { FooterComponent } from '../HeaderFooter/footer/footer.component';
import { MobileMenuComponent } from '../mobile-menu/mobile-menu.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  imports: [ CommonModule, NgxSpinnerModule],
  exports : [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HeaderComponent,
    FooterComponent,
    MobileMenuComponent,
    NgxSpinnerModule
  ],
  providers: [],
  declarations: [ HeaderComponent, FooterComponent, MobileMenuComponent ],
})
export class SharedModule { }