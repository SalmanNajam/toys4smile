import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
declare var jQuery: any;
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
    user: User;
    @Input() isLoggedIn: boolean;

    constructor(private userService: UserService) {
        this.user = JSON.parse(localStorage.getItem('user'));
        let token = localStorage.getItem('access_token');
        if (token != null) {
            this.isLoggedIn = true;
        } else {
            this.isLoggedIn = false;
        }
        setTimeout(() => {
            (function ($) {
                "use strict";
                var $body = $('body'),
                    $wrapper = $('.body-innerwrapper'),
                    $toggler = $('#offcanvas-toggler'),
                    $close = $('.close-offcanvas'),
                    $offCanvas = $('.offcanvas-menu');

                $toggler.on('click', function (event) {
                    event.preventDefault();
                    stopBubble(event);
                    setTimeout(offCanvasShow, 50);
                });

                $close.on('click', function (event) {
                    event.preventDefault();
                    offCanvasClose();
                });

                var offCanvasShow = function () {
                    $body.addClass('offcanvas');
                    $wrapper.on('click', offCanvasClose);
                    $close.on('click', offCanvasClose);
                    $offCanvas.on('click', stopBubble);

                };

                var offCanvasClose = function () {
                    $body.removeClass('offcanvas');
                    $wrapper.off('click', offCanvasClose);
                    $close.off('click', offCanvasClose);
                    $offCanvas.off('click', stopBubble);
                };

                var stopBubble = function (e) {
                    e.stopPropagation();
                    return true;
                };

                offCanvasClose();

            })(jQuery);

        }, 0);
    }

    ngOnInit() {
    }
}
