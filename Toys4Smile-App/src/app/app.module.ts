import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http'
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppComponent } from './app.component';
import { LogoutComponent } from './logout/logout.component';
import { UserGuard } from './core/services/user-guard.service';
import { AdminGuard } from './core/services/admin-guard.service';
import { AuthGuard } from './core/services/auth-guard.service';
import { UserService } from './services/user.service';
import { UserServicee } from './user.servicee';
import { DonationService } from './donation.service';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { SharedModule } from './shared/shared.module';
import { ImageCompressService } from 'ng2-image-compress';

@NgModule({
  declarations: [
    AppComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    SharedModule,
    ToastModule.forRoot()
  ],
  exports: [NgxSpinnerModule, AppRoutingModule],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy },
    UserGuard, AdminGuard, AuthGuard, UserService, UserServicee, DonationService, ImageCompressService],
  bootstrap: [AppComponent]
})
export class AppModule { }
