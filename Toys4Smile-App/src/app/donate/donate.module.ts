import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { DonateRoutingModule } from "./donate-routing.module";
import { DonateComponent } from "./donate.component";

@NgModule({
  imports: [CommonModule, DonateRoutingModule, SharedModule],
  declarations: [DonateComponent]
})

export class DonateModule { }