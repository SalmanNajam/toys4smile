import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DonateComponent } from "./donate.component";

const routes: Routes = [
    { path: "", component: DonateComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class DonateRoutingModule { }