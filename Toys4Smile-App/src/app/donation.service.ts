import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { donationViewModel } from '../app/models/donationViewModel';
import { Observable } from 'rxjs';
import { donationByIdViewModel } from './models/donationByIdViewModel';
import { imagePaths } from './models/imagePaths';

@Injectable()

export class DonationService {

    constructor(private httpClient: HttpClient) { }

    getDonationsById(id: number): Observable<donationViewModel[]> {
        return this.httpClient.get<donationViewModel[]>('http://api.toys4smile.com/api/Donations/getDonationsByUserId/' + id);
    }

    getAllDonation(): Observable<donationViewModel[]> {
        return this.httpClient.get<donationViewModel[]>('http://api.toys4smile.com/api/Donations/getAllDonations');
    }

    getAllDonationByStatus(status: string): Observable<donationViewModel[]> {
        return this.httpClient.get<donationViewModel[]>('http://api.toys4smile.com/api/Donations/getAllDonationsByStatus/' + status);
    }

    getAllReceivedDonationsForOrphanage(status: string): Observable<donationViewModel[]> {
        return this.httpClient.get<donationViewModel[]>('http://api.toys4smile.com/api/Donations/getAllReceivedDonationsForOrphanage/' + status);
    }

    getAllDonationsByOrphanageId(orphanageId: number): Observable<donationViewModel[]> {
        return this.httpClient.get<donationViewModel[]>('http://api.toys4smile.com/api/Donations/getAllDonationsByOrphanageId/' + orphanageId);
    }

    getDonationByTrackingId(trackingId: string): Observable<donationViewModel> {
        return this.httpClient.get<donationViewModel>('http://api.toys4smile.com/api/Donations/getDonationByTrackingId/' + trackingId);
    }

    getAllDonationByTrackingId(trackingId: string): Observable<donationViewModel> {
        return this.httpClient.get<donationViewModel>('http://api.toys4smile.com/api/Donations/getAllDonationsByTrackingId/' + trackingId);
    }

    updateDonationStatus(value: donationViewModel) {
        return this.httpClient.post('http://api.toys4smile.com/api/Donations/updateDonationStatus/', value);
    }

    deleteDonation(value: donationViewModel): Observable<string> {
        return this.httpClient.post<string>('http://api.toys4smile.com/api/Donations/deleteDonation', value);
    }

    getDonationByDonatoinId(donationId: number): Observable<donationByIdViewModel> {
        return this.httpClient.get<donationByIdViewModel>('http://api.toys4smile.com/api/Donations/getDonationByDonatoinId/' + donationId);
    }

    getDonationImagesByDonationId(donationId: number): Observable<imagePaths[]> {
        return this.httpClient.get<imagePaths[]>('http://api.toys4smile.com/api/Donations/getDonationImagesByDonationId/' + donationId);
    }

    deleteimage(value: imagePaths): Observable<boolean> {
        return this.httpClient.post<boolean>('http://api.toys4smile.com/api/Donations/deleteimage', value);
    }
}
