import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DisabledUserComponent } from "./disabled-user.component";
import { UserGuard } from "../core/services/user-guard.service";

const routes: Routes = [
    { path: "", component: DisabledUserComponent, canActivate: [UserGuard]},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class DisabledUserRoutingModule { }