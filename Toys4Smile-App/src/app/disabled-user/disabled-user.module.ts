import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { DisabledUserRoutingModule } from "./disabled-user-routing.module";
import { DisabledUserComponent } from "./disabled-user.component";

@NgModule({
    imports: [CommonModule, DisabledUserRoutingModule, SharedModule],
    declarations: [DisabledUserComponent]
})

export class DisabledUserModule { }