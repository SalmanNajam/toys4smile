import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ForgotpasswordComponent } from "./forgotpassword.component";

const routes: Routes = [
    { path: "", component: ForgotpasswordComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class ForgotPasswordRoutingModule { }
