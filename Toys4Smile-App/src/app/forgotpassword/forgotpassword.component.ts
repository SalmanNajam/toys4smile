import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '../../../node_modules/@angular/forms';
import { Email } from '../models/email.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from '../models/user.model';
declare var jQuery: any;

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html'
})

export class ForgotpasswordComponent implements OnInit {
  isLoggedIn: boolean;
  status: boolean;
  message = "";
  form: FormGroup;
  email: Email = new Email();
  user: User;

  constructor(private fb: FormBuilder, private userService: UserService,
    private spinner: NgxSpinnerService) {
    this.user = JSON.parse(localStorage.getItem('user'));
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');

        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });

        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });

        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);
        };

        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };

        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };

        offCanvasClose();

      })(jQuery);

    }, 0);
  }

  ngOnInit() {
    this.status = false;
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email])
    })
  }

  resetPassword() {
    this.status = false;
    this.spinner.show();
    this.email.emailId = this.form.value.email;
    this.userService.forgotPassword(this.email).subscribe(
      (response) => {
        if (response === false) {
          this.status = true;
          this.spinner.hide();
          this.message = "No account is associated with this email id.";
        } else if (response === true) {
          this.spinner.hide();
          this.status = true;
          this.message = "Password has been reset. Please check your email!";
        }
      }
    )
  }
}