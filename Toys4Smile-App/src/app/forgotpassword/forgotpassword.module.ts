import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { ForgotPasswordRoutingModule } from "./forgotpassword-routing.module";
import { ForgotpasswordComponent } from "./forgotpassword.component";

@NgModule({
  imports: [CommonModule, ForgotPasswordRoutingModule, SharedModule],
  declarations: [ForgotpasswordComponent]
})

export class ForgotPasswordModule { }