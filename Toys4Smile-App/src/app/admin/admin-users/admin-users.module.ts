import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { AdminUsersRoutingModule } from "./admin-users-routing.module";
import { AdminUsersComponent } from "./admin-users.component";

@NgModule({
  imports: [CommonModule, AdminUsersRoutingModule, SharedModule],
  declarations: [AdminUsersComponent]
})

export class AdminUsersModule {}