import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminUsersComponent } from "./admin-users.component";

const routes: Routes = [
    { path: "", component: AdminUsersComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminUsersRoutingModule { }