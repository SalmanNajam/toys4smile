import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from '../../models/user.model';
import { Email } from '../../models/email.model';
import { UserService } from '../../services/user.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var jQuery: any;

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html'
})

export class AdminUsersComponent implements OnInit {
  users: User[];
  user: User;
  searchedUser: User;
  editUser: User;
  deleteUser: User;
  isLoggedIn: boolean;
  selectedUserRole: number;
  roleName: string = 'Donor';
  searchUser: FormGroup;
  email: Email = new Email();

  constructor(private userService: UserService, private spinner: NgxSpinnerService,
    private fb: FormBuilder, private router: Router, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    this.user = JSON.parse(localStorage.getItem('user'));
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);
        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);

    this.createForm();
  }

  ngOnInit() {
    this.selectedUserRole = 3;
    this.userService.getAllUsersByRoleId(this.selectedUserRole).subscribe(
      (response) => {
        this.users = response;
      },
      (errors) => {
        this.toastr.error('An error occurred while fetching user records... Please try again later!');
      }
    );
  }

  createForm() {
    this.searchUser = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    })
  }

  public onChange(value: number): void {
    this.spinner.show();
    this.selectedUserRole = value;
    if (value == 2) {
      this.roleName = 'Admin';
    } else if (value == 3) {
      this.roleName = 'Donor';
    } else if (value == 4) {
      this.roleName = 'Guest';
    } else if (value == 5) {
      this.roleName = 'Orphanage';
    }
    this.userService.getAllUsersByRoleId(value).subscribe(
      (response) => {
        this.spinner.hide();
        this.users = response;
        this.users = this.users.filter(obj => obj.UserId !== this.user.UserId);
      },
      (errors) => {
        this.spinner.hide();
      }
    );
    this.selectedUserRole = value;
  }

  public onChange1(isEnabled: boolean, user: any): void {
    this.spinner.show();
    this.editUser = user;
    this.editUser.status = isEnabled;
    this.userService.changeUserStatus(this.editUser).subscribe(
      (response) => {
        if (response === true) {
          this.spinner.hide();
          this.toastr.success('User status has been changed successfully!');
        } else {
          this.toastr.error('An error occurred while changing user status... Please try again later!');
        }
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.error('An error occurred while changing user status... Please try again later!');
      }
    );
  }

  userToDelete(userToDelete: any) {
    this.deleteUser = userToDelete;
  }

  onDelete() {
    this.spinner.show();
    this.userService.deleteUser(this.deleteUser).subscribe(
      (response) => {
        if (response == true) {
          this.spinner.hide();
          this.toastr.success('The user has been deleted successfully!');
          this.userService.getAllUsersByRoleId(this.selectedUserRole).subscribe(
            (response) => {
              this.users = response;
            },
            (errors) => {
              this.toastr.error('An error occurred while fetching users record... Please try again later!');
            }
          );
        } else {
          this.toastr.error('An error occurred while deleting user record... Please try again later!');

        }
      },
      (errors) => {
        this.toastr.error('An error occurred while deleting user record... Please try again later!');
      }
    );
  }

  searchUserByEmail() {
    if (this.searchUser.value.email === null || this.searchUser.value.email === undefined || this.searchUser.value.email === '') {
      return;
    }
    const formModel = this.searchUser.value;
    this.email.emailId = formModel.email;
    this.userService.GetUserByEmail(this.email).subscribe(
      (response) => {
        this.searchedUser = response;
        if (this.searchedUser.UserId !== null && this.searchedUser.UserId !== undefined) {
          this.router.navigate(['/admin/admin-track-user/' + this.searchedUser.UserId]);
        }
      },
      (errors) => {
        this.toastr.error('An error occurred while fetching user record... Please try again later!');
      }
    )
  }
}
