import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAddUserComponent } from "./admin-add-user.component";

const routes: Routes = [
    { path: "", component: AdminAddUserComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminAddUserRoutingModule { }