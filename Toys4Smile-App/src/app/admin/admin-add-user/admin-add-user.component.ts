import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { adminUserModel } from '../../models/adminUserModel';
import { User } from '../../models/user.model';
import { Email } from '../../models/email.model';
import { UserServicee } from '../../user.servicee';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
declare var jQuery: any;

@Component({
  selector: 'app-admin-add-user',
  templateUrl: './admin-add-user.component.html'
})

export class AdminAddUserComponent implements OnInit {
  user: adminUserModel = new adminUserModel();
  isLoggedIn: boolean;
  canAddAmin: boolean;
  loggedInUser: User;
  isOrphanage: boolean;
  userRole: string = "";
  numberPrefix: string;
  email: Email = new Email();
  isEmailAvailable: boolean;
  registerForm: FormGroup;
  userr: User;


  constructor(private fb: FormBuilder, private userService: UserServicee,
    private spinner: NgxSpinnerService, public toastr: ToastsManager, vcr: ViewContainerRef,
    private router: Router) {
    this.userr = JSON.parse(localStorage.getItem('user'));
    this.toastr.setRootViewContainerRef(vcr);
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);
        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
    this.isOrphanage = true;
    this.createForm();
    this.loggedInUser = JSON.parse(localStorage.getItem('user'));
    this.registerForm.controls['ngoName'].disable();
    this.isEmailAvailable = false;
  }

  onChangeNumberPrefix(nmbrPrefix: string) {
    this.numberPrefix = nmbrPrefix;
  }

  createForm() {
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required]
      , lastName: ['', Validators.required]
      , email: ['', [Validators.required, Validators.email]]
      , mobile: ['', Validators.required]
      , password: ['', [Validators.required, Validators.minLength(8)]]
      , address: ['', Validators.required]
      , userRole: ['', Validators.required]
      , countrycode: ['', Validators.required]
      , ngoName: ['']
      , dob: ['']
    })
  }

  ngOnInit() {
    if (this.loggedInUser.RoleId === 1) {
      this.canAddAmin = true
    } else {
      this.canAddAmin = false;
    }
  }

  onChange(value: string) {
    if (value === 'Orphanage') {
      this.isOrphanage = false;
      this.registerForm.controls['ngoName'].enable();
    } else {
      this.registerForm.controls["ngoName"].setValue('');
      this.registerForm.controls['ngoName'].disable();
    }
  }

  addUser() {
    this.spinner.show();
    const formModel = this.registerForm.value;
    this.user.firstName = formModel.firstName;
    this.user.lastName = formModel.lastName;
    this.user.email = formModel.email;
    this.user.mobile = formModel.mobile;
    this.user.dob = formModel.dob;
    this.user.password = formModel.password;
    this.user.address = formModel.address;
    this.user.RoleName = formModel.userRole;
    this.user.ngoName = formModel.ngoName;
    this.user.countrycode = formModel.countrycode;
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required]
      , lastName: ['', Validators.required]
      , email: ['', [Validators.required, Validators.email]]
      , dob: ['', Validators.required]
      , mobile: ['', Validators.required]
      , password: ['', Validators.required]
      , address: ['', Validators.required]
      , userRole: ['', Validators.required]
      , countrycode: ['', Validators.required]
    })

    setTimeout(() => {
      this.router.navigate(['/admin/admin-users']);
    }, 1000);
    this.userService.registerUserAsAdmin(this.user).subscribe(
      (response) => {
        if (response === true) {
          this.spinner.hide();
          setTimeout(() => {
            this.router.navigate(['/admin/admin-users']);
          }, 2000);
          this.toastr.success('New user has been added successfully!');
        } else {
          this.spinner.hide();
          this.toastr.error('An error occurred while adding a new user... Please try again later!');
        }
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.error('An error occurred while adding a new user... Please try again later!');
      }
    );
  }

  focusOutFunction() {
    this.email.emailId = this.registerForm.value.email;
    this.userService.hasEmailRegistered(this.email).subscribe(
      (response) => {
        if (response === true) {
          this.isEmailAvailable = true;
          this.registerForm.controls["email"].setValue('');
        } else if (response === false) {
          this.isEmailAvailable = false;
        }
      },
      (errors) => {
      }
    )
  }
}
