import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AdminAddUserRoutingModule } from "./admin-add-user-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { AdminAddUserComponent } from "./admin-add-user.component";

@NgModule({
  imports: [CommonModule, AdminAddUserRoutingModule, SharedModule],
  declarations: [AdminAddUserComponent]
})

export class AdminAddUserModule { }