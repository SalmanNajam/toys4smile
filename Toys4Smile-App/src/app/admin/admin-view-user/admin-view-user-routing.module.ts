import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminViewUserComponent } from "./admin-view-user.component";

const routes: Routes = [
    { path: "", component: AdminViewUserComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminViewUserRoutingModule { }