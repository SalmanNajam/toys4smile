import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { adminUserModel } from '../../models/adminUserModel';
import { UserService } from '../../services/user.service';
declare var jQuery: any;

@Component({
  selector: 'app-admin-view-user',
  templateUrl: './admin-view-user.component.html'
})

export class AdminViewUserComponent implements OnInit {
  user: adminUserModel = new adminUserModel();
  isLoggedIn: boolean;
  
  constructor(private route: ActivatedRoute, private userSerice: UserService) {
    let token = localStorage.getItem('access_token');
    if (token != null) {
        this.isLoggedIn = true;
    } else {
        this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);

        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
   }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userSerice.getUserByUserId(id).subscribe(x => this.user = x);
  }
}
