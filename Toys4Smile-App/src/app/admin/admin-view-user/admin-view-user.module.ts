import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { AdminViewUserComponent } from "./admin-view-user.component";
import { AdminViewUserRoutingModule } from "./admin-view-user-routing.module";

@NgModule({
  imports: [CommonModule, AdminViewUserRoutingModule, SharedModule],
  declarations: [AdminViewUserComponent]
})

export class AdminViewUserModule {}