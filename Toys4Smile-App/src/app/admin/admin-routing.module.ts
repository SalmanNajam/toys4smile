import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AuthGuard } from '../core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '', component: AdminComponent, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'admin-donations', pathMatch: 'full' },
      { path: 'admin-donations', loadChildren: './admin-donations/admin-donations.module#AdminDonationsModule' },
      { path: 'admin-users', loadChildren: './admin-users/admin-users.module#AdminUsersModule' },
      { path: 'admin-add-user', loadChildren: './admin-add-user/admin-add-user.module#AdminAddUserModule' },
      { path: 'add-donation', loadChildren: './admin-add-donation/admin-add-donation.module#AdminAddDonationModule' },
      { path: 'edit-donation/:id', loadChildren: './admin-edit-donation/admin-edit-donation.module#AdminEditDonationModule' },
      { path: 'edit-user/:id', loadChildren: './admin-edit-user/admin-edit-user.module#AdminEditUserModule' },
      { path: 'admin-track-id/:id', loadChildren: './admin-track-id/admin-track-id.module#AdminTrackIdModule' },
      { path: 'admin-track-user/:id', loadChildren: './admin-track-user/admin-track-user.module#AdminTrackIdModule' },
      { path: 'admin-view-donation/:id', loadChildren: './admin-view-donation/admin-view-donation.module#AdminViewDonationModule' },
      { path: 'view-user/:id', loadChildren: './admin-view-user/admin-view-user.module#AdminViewUserModule' }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
