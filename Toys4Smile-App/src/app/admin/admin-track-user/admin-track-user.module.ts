import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { AdminTrackUserComponent } from "./admin-track-user.component";
import { AdminTrackUserRoutingModule } from "./admin-track-user-routing.module";

@NgModule({
  imports: [CommonModule, AdminTrackUserRoutingModule, SharedModule],
  declarations: [AdminTrackUserComponent]
})

export class AdminTrackIdModule {}