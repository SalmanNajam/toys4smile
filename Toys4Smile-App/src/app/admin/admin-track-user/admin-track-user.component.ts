import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { adminUserModel } from '../../models/adminUserModel';
import { User } from '../../models/user.model';
import { Email } from '../../models/email.model';
import { UserService } from '../../services/user.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var jQuery: any;

@Component({
  selector: 'app-admin-track-user',
  templateUrl: './admin-track-user.component.html'
})
export class AdminTrackUserComponent implements OnInit {
  user: adminUserModel;
  loggedInUser: User;
  userId: number;
  searchUser: FormGroup;
  isLoggedIn: boolean;
  searchedUser: User;
  email: Email = new Email();
  editUser: User;
  deleteUser: User;
  selectedUserRole: number;
  users: User[];

  constructor(private route: ActivatedRoute, private userService: UserService, private fb: FormBuilder,
    private router: Router, private spinner: NgxSpinnerService, public toastr: ToastsManager, vcr: ViewContainerRef) {
      this.toastr.setRootViewContainerRef(vcr);
      let token = localStorage.getItem('access_token');
    if (token != null) {
        this.isLoggedIn = true;
    } else {
        this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);

        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
    route.params.subscribe(val => {
      this.loggedInUser = JSON.parse(localStorage.getItem('user'));
      this.userId = +this.route.snapshot.paramMap.get('id');
      this.userService.getUserByUserId(this.userId).subscribe(
        (response) => {
          this.user = response;
        }
      )
    });
    this.createForm();
  }

  ngOnInit() { }

  createForm() {
    this.searchUser = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    })
  }

  userToDelete(userToDelete: any) {
    this.deleteUser = userToDelete;
  }

  onDelete() {
    this.spinner.show();
    this.userService.deleteUser(this.deleteUser).subscribe(
      (response) => {
        if (response == true) {
          this.spinner.hide();
          this.toastr.success('The user has been deleted successfully!');
          setTimeout(() => {
            this.router.navigate(['/admin/admin-users']);
          }, 2000);
        } else {
          this.toastr.error('An error occurred while deleting user record... Please try again later!');
        }
      },
      (errors) => {
        this.toastr.error('An error occurred while deleting user record... Please try again later!');
      }
    );
  }

  searchUserByEmail() {
    this.spinner.show();
    if (this.searchUser.value.email === null || this.searchUser.value.email === undefined || this.searchUser.value.email === '') {
      return;
    }
    const formModel = this.searchUser.value;
    this.email.emailId = formModel.email;
    this.userService.GetUserByEmail(this.email).subscribe(
      (response) => {
        this.spinner.hide();
        if (response !== null) {
          this.searchedUser = response;
          this.router.navigate(['/admin/admin-track-user/' + this.searchedUser.UserId]);
          this.toastr.success('One user record found!');
        } else {
          this.spinner.hide();
          this.user = null;
          this.toastr.error('No user record found!');
        }
      },
      (errors) => {
        this.toastr.error('An error occurred while fetching user record... Please try again later!');
      }
    )
  }

  public onChange1(isEnabled: boolean, user: any): void {
    this.spinner.show();
    this.editUser = user;
    this.editUser.status = isEnabled;
    this.userService.changeUserStatus(this.editUser).subscribe(
      (response) => {
        if (response === true) {
          this.spinner.hide();
          this.toastr.success('User status has been changed successfully!');
        } else {
          this.toastr.error('An error occurred while changing user status... Please try again later!');
        }
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.error('An error occurred while changing user status... Please try again later!');
      }
    );
  }
}
