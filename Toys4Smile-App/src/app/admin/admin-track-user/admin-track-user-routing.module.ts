import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminTrackUserComponent } from "./admin-track-user.component";

const routes: Routes = [
    { path: "", component: AdminTrackUserComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminTrackUserRoutingModule { }