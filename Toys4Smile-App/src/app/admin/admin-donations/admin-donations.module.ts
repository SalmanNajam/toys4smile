import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AdminDonationsRoutingModule } from "./admin-donations-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { AdminDonationsComponent } from "./admin-donations.component";
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  imports: [CommonModule, AdminDonationsRoutingModule, SharedModule],
  declarations: [AdminDonationsComponent]
})

export class AdminDonationsModule {}