import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { donationViewModel } from '../../models/donationViewModel';
import { User } from '../../models/user.model';
import { OrphanageTitles } from '../../models/orphanageTitles.model';
import { DonationService } from '../../donation.service';
import { UserService } from '../../services/user.service';
declare var jQuery: any;

@Component({
  selector: 'app-admin-donations',
  templateUrl: './admin-donations.component.html'
})

export class AdminDonationsComponent implements OnInit {
  donations: donationViewModel[];
  donation: donationViewModel = new donationViewModel();
  deleteId: donationViewModel;
  ID: number;
  donationStatus = 'Waiting for collection';
  searchStatus: string;
  previousStatus = '';
  afterdeleteState = '';
  isLoggedIn: boolean;
  user: User;
  trackResult: donationViewModel;
  orphanageTitles: OrphanageTitles[];
  searchDonation: FormGroup;

  constructor(private fb: FormBuilder, private donationService: DonationService,
    private userService: UserService, private spinner: NgxSpinnerService,
    private router: Router) {
      this.user = JSON.parse(localStorage.getItem('user'));
      let token = localStorage.getItem('access_token');
      if (token != null) {
          this.isLoggedIn = true;
      } else {
          this.isLoggedIn = false;
      }
      setTimeout(() => {
        (function ($) {
          "use strict";
          var $body = $('body'),
            $wrapper = $('.body-innerwrapper'),
            $toggler = $('#offcanvas-toggler'),
            $close = $('.close-offcanvas'),
            $offCanvas = $('.offcanvas-menu');
  
          $toggler.on('click', function (event) {
            event.preventDefault();
            stopBubble(event);
            setTimeout(offCanvasShow, 50);
          });
  
          $close.on('click', function (event) {
            event.preventDefault();
            offCanvasClose();
          });
  
          var offCanvasShow = function () {
            $body.addClass('offcanvas');
            $wrapper.on('click', offCanvasClose);
            $close.on('click', offCanvasClose);
            $offCanvas.on('click', stopBubble);
          };
  
          var offCanvasClose = function () {
            $body.removeClass('offcanvas');
            $wrapper.off('click', offCanvasClose);
            $close.off('click', offCanvasClose);
            $offCanvas.off('click', stopBubble);
          };
  
          var stopBubble = function (e) {
            e.stopPropagation();
            return true;
          };
  
          offCanvasClose();
  
        })(jQuery);
  
      }, 0);
    this.getAllDonationByStatus();
    this.createForm();
  }


  createForm() {
    this.searchDonation = this.fb.group({
      trackingId: ['', Validators.required]
    })
  }

  onOrphanageTitleSelect(orphanageId: number) { }

  getAllDonationByStatus() {
    this.spinner.show();
    this.donationService.getAllDonationByStatus('Waiting for collection').subscribe(
      (response) => {
        this.spinner.hide();
        this.donations = response;
      },
      (errors) => {
        this.spinner.hide();
      }
    );
  }

  public onChange(value): void {
    this.spinner.show();
    this.searchStatus = value;
    this.donationService.getAllDonationByStatus(value).subscribe(
      (response) => {
        this.spinner.hide();
        this.donations = response;
      },
      (errors) => {
        this.spinner.hide();
      }
    );
    this.donationStatus = value;
    this.searchStatus = value;
  }

  public onChange1(value1, id: any): void {
    this.previousStatus = this.donationStatus;
    this.donation.DonationId = id.DonationId;
    this.donation.status = value1;
    this.spinner.show();
    this.donationService.updateDonationStatus(this.donation).subscribe(
      (response) => {
        this.donationService.getAllDonationByStatus(this.previousStatus).subscribe(
          (response) => {
            this.spinner.hide();
            this.donations = response;
          },
          (errors) => {
            this.spinner.hide();
          }
        );
      },
      (errors) => {
        this.spinner.hide();
      }
    );
  }

  getDonationId(id: number) {
    this.ID = id;
  }

  onDelete(id: any) {
    this.deleteId = id;
  }

  deleteDonation() {
    this.spinner.show();
    this.donationService.deleteDonation(this.deleteId).subscribe(
      (response) => {
        if (response !== '') {
          this.spinner.hide();
          this.afterdeleteState = response;
          this.donationService.getAllDonationByStatus(this.afterdeleteState).subscribe(
            (response) => {
              this.spinner.hide();
              this.donations = response;
            },
            (errors) => {
              this.spinner.hide();
            }
          );
        }
      },
      (errors) => {
        this.spinner.hide();
      }
    );
  }

  ngOnInit() {
    this.userService.GetOrphanageTitles().subscribe(
      (response) => {
        this.orphanageTitles = response;
        console.log(response);
      },
      (errors) => {
        console.log(errors);
      }
    )
  }

  searchDonations() {
    this.spinner.show();
    const formModel = this.searchDonation.value;
    var searchTrackingId = formModel.trackingId as string;
    this.donationService.getDonationByTrackingId(searchTrackingId).subscribe(
      (response) => {
        this.spinner.hide();
        if (response != null) {
          this.trackResult = response;
          this.router.navigate(['/admin/admin-track-id/' + this.trackResult.TrackingId]);
        } else {
          this.spinner.hide();
        }
      },
      (errors) => {
        this.spinner.hide();
      }
    );
  }
}
