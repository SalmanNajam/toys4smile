import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminDonationsComponent } from "./admin-donations.component";

const routes: Routes = [
    { path: "", component: AdminDonationsComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminDonationsRoutingModule { }