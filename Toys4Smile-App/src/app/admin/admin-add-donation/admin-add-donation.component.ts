import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { OrphanageTitles } from '../../models/orphanageTitles.model';
import { UserServicee } from '../../user.servicee';
import { UserService } from '../../services/user.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ImageCompressService, ImageUtilityService, IImage } from "ng2-image-compress";
import { User } from '../../models/user.model';
declare var jQuery: any;
let images: Array<IImage> = [];

@Component({
  selector: 'app-admin-add-donation',
  templateUrl: './admin-add-donation.component.html'
})
export class AdminAddDonationComponent implements OnInit {
  orphanageTitles: OrphanageTitles[];
  isLoggedIn: boolean;
  numberPrefix: string;
  adminDonorForm: FormGroup;
  files: any;
  selectedFile: File[] = [];
  urls = new Array<string>();
  selectedImage: any;
  processedImages: any = [];
  user: User;

  constructor(private fb: FormBuilder, private userServicee: UserServicee,
    private userService: UserService, public toastr: ToastsManager, vcr: ViewContainerRef,
    private spinner: NgxSpinnerService, private router: Router, private imgCompressService: ImageCompressService) {
      this.toastr.setRootViewContainerRef(vcr);
      this.user = JSON.parse(localStorage.getItem('user'));
      let token = localStorage.getItem('access_token');
      if (token != null) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
      setTimeout(() => {
        (function ($) {
          "use strict";
          var $body = $('body'),
            $wrapper = $('.body-innerwrapper'),
            $toggler = $('#offcanvas-toggler'),
            $close = $('.close-offcanvas'),
            $offCanvas = $('.offcanvas-menu');
          $toggler.on('click', function (event) {
            event.preventDefault();
            stopBubble(event);
            setTimeout(offCanvasShow, 50);
          });
          $close.on('click', function (event) {
            event.preventDefault();
            offCanvasClose();
          });
          var offCanvasShow = function () {
            $body.addClass('offcanvas');
            $wrapper.on('click', offCanvasClose);
            $close.on('click', offCanvasClose);
            $offCanvas.on('click', stopBubble);
          };
          var offCanvasClose = function () {
            $body.removeClass('offcanvas');
            $wrapper.off('click', offCanvasClose);
            $close.off('click', offCanvasClose);
            $offCanvas.off('click', stopBubble);
          };
          var stopBubble = function (e) {
            e.stopPropagation();
            return true;
          };
          offCanvasClose();
        })(jQuery);
      }, 0);
    this.createForm();
  }

  createForm() {
    this.adminDonorForm = this.fb.group({
      fullName: [''],
      email: ['', Validators.required]
      , mobile: ['', Validators.required]
      , noOfToys: ['', Validators.required]
      , countrycode: ['', Validators.required]
      , orphanageTitle: ['', Validators.required]
      , specialComments: ['']
      , address: ['']
    })
  }

  onChangeNumberPrefix(nmbrPrefix: string) {
    this.numberPrefix = nmbrPrefix;
  }

  ngOnInit() {
    this.userService.GetOrphanageTitles().subscribe(
      (response) => {
        this.orphanageTitles = response;
      },
      (errors) => {
        this.toastr.error('Unable to fetch orphanage account details... Please try again later!');
      }
    )
  }

  imageOptimizied(event) {
    this.processedImages = [];
    let files: File[];
    let observer = ImageUtilityService.filesToSourceImages(event.target.files);
    observer.subscribe((image) => {
      images.push(image);
    }, null, () => {
      var files: IImage[] = images;
      let image_files: IImage[] = [];
      let nonImage_files: IImage[] = [];
      for (var i = 0; i < files.length; i++) {
        if (String(files[i].imageDataUrl).toUpperCase().indexOf("DATA:IMAGE/PNG;") !== -1 ||
          String(files[i].imageDataUrl).toUpperCase().indexOf("DATA:IMAGE/JPEG;") !== -1) {
          image_files.push(files[i]);
        }
        else {
          nonImage_files.push(files[i]);
        }
      };
      ImageCompressService.IImageListToCompressedImageSource(images).then(imagesResult => {
        this.processedImages = imagesResult;
      })
    });
  }

  deleteImage(item: any) {
    for (var i = this.processedImages.length - 1; i >= 0; i--) {
      if (this.processedImages[i] == item) {
       
        this.processedImages.splice(i, 1);
      }
    }
  }

  addDonation() {
    this.spinner.show();
    const formModel = this.adminDonorForm.value;
    let formData: FormData = new FormData();
    let oneFile = this.selectedFile[0];
    formData.append('fullName', formModel.fullName as string);
    formData.append('mobile', formModel.mobile);
    formData.append('noOfToys', formModel.noOfToys);
    formData.append('email', formModel.email);
    formData.append('countrycode', formModel.countrycode);
    formData.append('selectedOrphanageId', formModel.orphanageTitle);
    formData.append('specialComments', formModel.specialComments);
    formData.append('address', formModel.address);
    for (var i = 0; i < this.processedImages.length; i++) {
      var blob = new Blob([this.processedImages[i].compressedImage.imageDataUrl], { type: 'image/jpg' });
      formData.append('MyFile' + i, blob, this.processedImages[i].compressedImage.imageDataUrl);
    }
    this.createForm();
    this.userServicee.donateAsAdmin(formData).subscribe(
      (response) => {
        if (response === true) {
          this.spinner.hide();
          this.toastr.success('Donation successful!');
          setTimeout(() => {
            this.router.navigate(['/admin/admin-donations']);
          }, 2000);
        } else {
          this.spinner.hide();
          this.toastr.error('An error occurred while submitting your donation... Please try again later!');
        }
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.error('An error occurred while submitting your donation... Please try again later!');
      }
    );
  }
}
