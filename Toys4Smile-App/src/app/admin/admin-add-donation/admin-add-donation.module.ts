import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AdminAddDonationRoutingModule } from "./admin-add-donation-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { AdminAddDonationComponent } from "./admin-add-donation.component";

@NgModule({
  imports: [CommonModule, AdminAddDonationRoutingModule, SharedModule],
  declarations: [AdminAddDonationComponent]
})

export class AdminAddDonationModule {}