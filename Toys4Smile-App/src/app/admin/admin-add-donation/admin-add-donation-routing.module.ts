import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAddDonationComponent } from "./admin-add-donation.component";

const routes: Routes = [
    { path: "", component: AdminAddDonationComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminAddDonationRoutingModule { }