import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AdminEditUserRoutingModule } from "./admin-edit-user-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { AdminEditUserComponent } from "./admin-edit-user.component";

@NgModule({
  imports: [CommonModule, AdminEditUserRoutingModule, SharedModule],
  declarations: [AdminEditUserComponent]
})

export class AdminEditUserModule {}