import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { OrphanageTitles } from '../../models/orphanageTitles.model';
import { User } from '../../models/user.model';
import { Email } from '../../models/email.model';
import { adminUserModel } from '../../models/adminUserModel';
import { UserServicee } from '../../user.servicee';
import { UserService } from '../../services/user.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var jQuery: any;

@Component({
  selector: 'app-admin-edit-user',
  templateUrl: './admin-edit-user.component.html'
})

export class AdminEditUserComponent implements OnInit {
  userRole: string;
  isOrphanage: boolean;
  canAddAmin: boolean;
  orphanageTitles: OrphanageTitles[];
  numberPrefix: string = '';
  loggedInUser: User;
  email: Email = new Email();
  isEmailAvailable: boolean;
  registerForm: FormGroup;
  isLoggedIn: boolean;
  user: adminUserModel = new adminUserModel();
  users: adminUserModel = {
    UserId: 1,
    firstName: 'tahir',
    lastName: 'sas',
    email: 'sasda',
    mobile: '03232',
    address: 'sasa',
    RoleId: 1,
    RoleName: 'admin',
    date: '22/8/2018',
    dob: '22/8/2018',
    password: 'sada',
    ngoName: 'test orphanage title',
    countrycode: ''
  }

  constructor(private fb: FormBuilder, private userServicee: UserServicee,
    private userService: UserService, private route: ActivatedRoute,
    private userSerice: UserService, private spinner: NgxSpinnerService,
    private router: Router, public toastr: ToastsManager, vcr: ViewContainerRef) {
      this.toastr.setRootViewContainerRef(vcr);
      let token = localStorage.getItem('access_token');
    if (token != null) {
        this.isLoggedIn = true;
    } else {
        this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);
        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
    this.isEmailAvailable = false;
    this.loggedInUser = JSON.parse(localStorage.getItem('user'));
    this.isOrphanage = true;
    this.createForm();
  }

  createForm() {
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required]
      , lastName: ['', Validators.required]
      , email: ['', [Validators.required, Validators.email]]
      , dob: ['', Validators.required]
      , mobile: ['', Validators.required]
      , password: ['', Validators.required]
      , address: ['', Validators.required]
      , userRole: ['']
      , ngoName: ['', Validators.required]
      , countrycode: ['', Validators.required]
    })
  }

  ngOnInit() {
    this.userService.GetOrphanageTitles().subscribe(
      (response) => {
        this.orphanageTitles = response;
      },
      (errors) => {
        console.log(errors);
      }
    )
    this.getUserById();
    if (this.users.RoleId === 1) {
      this.canAddAmin = true
    } else {
      this.canAddAmin = false;
    }
  }

  focusOutFunction() {
    this.email.emailId = this.registerForm.value.email;
    this.userServicee.hasEmailRegistered(this.email).subscribe(
      (response) => {
        if (response === true) {
          this.isEmailAvailable = true;
          this.registerForm.controls["email"].setValue('');
        } else if (response === false) {
          this.isEmailAvailable = false;
        }
      },
      (errors) => {
      }
    )
  }

  getUserById() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userSerice.getUserByUserId(id).subscribe(
      (response) => {
        this.user = response;
        this.users.address = this.user.address;
        this.users.date = this.user.date.slice(0, 10);
        this.users.dob = this.user.dob.slice(0, 10);
        this.users.email = this.user.email;
        this.users.firstName = this.user.firstName;
        this.users.lastName = this.user.lastName;
        this.users.mobile = this.user.mobile;
        this.users.password = this.user.password;
        this.users.RoleId = this.user.RoleId;
        this.users.RoleName = this.user.RoleName;
        this.userRole = this.user.RoleName;
        this.users.UserId = this.user.UserId;
        this.users.ngoName = this.user.ngoName;
        this.numberPrefix = this.user.countrycode;
        this.users.countrycode = this.user.countrycode;
        this.registerForm.patchValue(this.users);
        if (this.users.RoleName === 'Orphanage') {
          this.isOrphanage = false;
          this.registerForm.controls['ngoName'].enable();
          this.registerForm.controls["ngoName"].setValue(this.users.ngoName);
        } else {
          this.registerForm.controls["ngoName"].setValue('');
          this.registerForm.controls['ngoName'].disable();
        }
      }
    );
  }

  onChangeNumberPrefix(nmbrPrefix: string) {
    this.numberPrefix = nmbrPrefix;
  }

  onChange(value: string) {
    if (value === 'Orphanage') {
      this.isOrphanage = false;
      this.registerForm.controls['ngoName'].enable();
      this.registerForm.controls["ngoName"].setValue(this.users.ngoName);
    } else {
      this.registerForm.controls["ngoName"].setValue('');
      this.registerForm.controls['ngoName'].disable();
    }
  }

  editForm() {
    this.users.address = this.user.address;
    this.users.date = this.user.date;
    this.users.dob = this.user.dob;
    this.users.email = this.user.email;
    this.users.firstName = this.user.firstName;
    this.users.lastName = this.user.lastName;
    this.users.mobile = this.user.mobile;
    this.users.countrycode = this.user.countrycode;
    this.users.password = this.user.password;
    this.users.RoleId = this.user.RoleId;
    this.users.RoleName = this.user.RoleName;
    this.users.UserId = this.user.UserId;
    this.users.ngoName = this.user.ngoName;
    this.registerForm.patchValue(this.users);
  }

  editUser() {
    this.spinner.show();
    const formModel = this.registerForm.value;
    this.user.firstName = formModel.firstName;
    this.user.lastName = formModel.lastName;
    this.user.email = formModel.email;
    this.user.mobile = formModel.mobile;
    this.user.dob = formModel.dob;
    this.user.password = formModel.password;
    this.user.address = formModel.address;
    this.user.RoleName = formModel.userRole;
    this.user.ngoName = formModel.ngoName;
    this.user.countrycode = this.numberPrefix;
    this.userServicee.editUserAsAdmin(this.user).subscribe(
      (response) => {
        this.spinner.hide();
        this.toastr.success('User details have been updated successfully!');
        this.createForm();
        setTimeout(() => {
          this.router.navigate(['/admin/admin-users']);
        }, 2000);
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.error('An error occurred while updating user details... Please try again later!');
      }
    );
  }
}
