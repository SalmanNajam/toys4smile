import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { imagePaths } from '../../models/imagePaths';
import { OrphanageTitles } from '../../models/orphanageTitles.model';
import { donationByIdViewModel } from '../../models/donationByIdViewModel';
import { adminEditDonationsViewModel } from '../../models/adminEditDonationsViewModel';
import { UserServicee } from '../../user.servicee';
import { UserService } from '../../services/user.service';
import { DonationService } from '../../donation.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ImageCompressService, ImageUtilityService, IImage } from "ng2-image-compress";
import { User } from '../../models/user.model';
declare var jQuery: any;
let images: Array<IImage> = [];

@Component({
  selector: 'app-admin-edit-donation',
  templateUrl: './admin-edit-donation.component.html'
})

export class AdminEditDonationComponent implements OnInit {
  loading: boolean;
  isLoggedIn: boolean;
  imagesList: imagePaths[];
  orphanageTitles: OrphanageTitles[];
  donor: donationByIdViewModel = new donationByIdViewModel();
  donorEdit: adminEditDonationsViewModel = new adminEditDonationsViewModel();
  numberPrefix: string = '';
  ngoId: number;
  files: any;
  selectedFile: File[] = [];
  urls = new Array<string>();
  selectedImage: any;
  processedImages: any = [];
  user: User;
  adminDonorForm: FormGroup;

  constructor(private fb: FormBuilder, private userServicee: UserServicee,
    private userService: UserService, private donationService: DonationService,
    private route: ActivatedRoute, private router: Router, private spinner: NgxSpinnerService,
    public toastr: ToastsManager, vcr: ViewContainerRef, private imgCompressService: ImageCompressService) {
      this.toastr.setRootViewContainerRef(vcr);
      this.user = JSON.parse(localStorage.getItem('user'));
      let token = localStorage.getItem('access_token');
      if (token != null) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
      setTimeout(() => {
        (function ($) {
          "use strict";
          var $body = $('body'),
            $wrapper = $('.body-innerwrapper'),
            $toggler = $('#offcanvas-toggler'),
            $close = $('.close-offcanvas'),
            $offCanvas = $('.offcanvas-menu');
          $toggler.on('click', function (event) {
            event.preventDefault();
            stopBubble(event);
            setTimeout(offCanvasShow, 50);
          });
          $close.on('click', function (event) {
            event.preventDefault();
            offCanvasClose();
          });
          var offCanvasShow = function () {
            $body.addClass('offcanvas');
            $wrapper.on('click', offCanvasClose);
            $close.on('click', offCanvasClose);
            $offCanvas.on('click', stopBubble);
          };
          var offCanvasClose = function () {
            $body.removeClass('offcanvas');
            $wrapper.off('click', offCanvasClose);
            $close.off('click', offCanvasClose);
            $offCanvas.off('click', stopBubble);
          };
          var stopBubble = function (e) {
            e.stopPropagation();
            return true;
          };
          offCanvasClose();
        })(jQuery);
        this.processedImages = [];
      }, 0);
    this.loading = false;
    this.createForm();
    this.userService.GetOrphanageTitles().subscribe(
      (response) => {
        this.orphanageTitles = response;
      },
      (errors) => {
        console.log(errors);
      }
    )
  }

  createForm() {
    this.adminDonorForm = this.fb.group({
      email: ['', Validators.required]
      , mobile: ['', Validators.required]
      , address: ['', Validators.required]
      , noOfToys: ['', Validators.required]
      , countrycode: ['', Validators.required]
      , specialComments: ['']
      , orphanageTitle: ['', Validators.required]
      , trackingid: ['']
    })
  }
  ngOnInit() {
    this.getProductById();
  }

  deleteImagee(s: imagePaths) {
    this.donationService.deleteimage(s).subscribe(
      (response) => {
        if (response === true) {
          this.toastr.success('The image has been deleted successfully!');
          this.getProductById();
        } else {
          this.toastr.error('An error occurred while deleting image... Please try again later!');
        }
      }
    );
  }

  imageOptimizied(event) {
    this.processedImages = [];
    let files: File[];
    let observer = ImageUtilityService.filesToSourceImages(event.target.files);
    observer.subscribe((image) => {
      images.push(image);
    }, null, () => {
      var files: IImage[] = images;
      let image_files: IImage[] = [];
      let nonImage_files: IImage[] = [];
      for (var i = 0; i < files.length; i++) {
        if (String(files[i].imageDataUrl).toUpperCase().indexOf("DATA:IMAGE/PNG;") !== -1 ||
          String(files[i].imageDataUrl).toUpperCase().indexOf("DATA:IMAGE/JPEG;") !== -1) {
          image_files.push(files[i]);
        }
        else {
          nonImage_files.push(files[i]);
        }
      };
      ImageCompressService.IImageListToCompressedImageSource(images).then(imagesResult => {
        this.processedImages = imagesResult;
      })
    });
  }

  deleteImage(item: any) {
    for (var i = this.processedImages.length - 1; i >= 0; i--) {
      if (this.processedImages[i] == item) {
        this.processedImages.splice(i, 1);
      }
    }
  }

  getProductById() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.donationService.getDonationByDonatoinId(id).subscribe(
      (response) => {
        console.log(response);
        this.donor = response;
        this.donorEdit.email = this.donor.email;
        this.donorEdit.mobile = this.donor.mobile;
        this.donorEdit.noOfToys = this.donor.noOfToysDonated;
        this.donorEdit.address = this.donor.address;
        this.donorEdit.trackingid = this.donor.TrackingId;
        this.adminDonorForm.controls['email'].disable();
        this.adminDonorForm.controls['trackingid'].disable();
        this.ngoId = this.donor.orphanageId;
        this.donorEdit.specialComments = this.donor.specialComments;
        this.donorEdit.countrycode = this.donor.countrycode;
        this.numberPrefix = this.donor.countrycode;
        this.adminDonorForm.patchValue(this.donorEdit);
        this.adminDonorForm.controls['orphanageTitle'].setValue(this.ngoId);
        this.donationService.getDonationImagesByDonationId(this.donor.DonationId).subscribe(
          (response) => {
            this.imagesList = response;
          },
          (errors) => {
            this.toastr.error('An error occurred while fetching donation details... Please try again later!');
          }
        )
      },
      (errors) => {
        this.toastr.error('An error occurred while fetching donation details... Please try again later!');
      }
    );
  }

  onChangeNumberPrefix(nmbrPrefix: string) {
    this.numberPrefix = nmbrPrefix;
  }

  editDonation() {
    this.spinner.show();
    const formModel = this.adminDonorForm.value;
    let formData: FormData = new FormData();
    let oneFile = this.selectedFile[0];
    formData.append('mobile', formModel.mobile);
    formData.append('noOfToys', formModel.noOfToys);
    formData.append('email', formModel.email);
    formData.append('donationId', this.donor.DonationId.toString());
    formData.append('userId', this.donor.UserId.toString());
    formData.append('address', formModel.address);
    formData.append('countrycode', formModel.countrycode);
    formData.append('selectedOrphanageId', formModel.orphanageTitle);
    formData.append('specialComments', formModel.specialComments);
    for (var i = 0; i < this.processedImages.length; i++) {
      var blob = new Blob([this.processedImages[i].compressedImage.imageDataUrl], { type: 'image/jpg' });
      formData.append('MyFile' + i, blob, this.processedImages[i].compressedImage.imageDataUrl);
    }
    this.userServicee.editDonationAsAdmin(formData).subscribe(
      (response) => {
        if (response === true) {
          this.spinner.hide();
          this.toastr.success('Donation details have been updated successfully!');
          this.createForm();
          setTimeout(() => {
            this.router.navigate(['/admin/admin-donations']);
          }, 2000);
        } else {
          this.spinner.hide();
          this.toastr.error('An error occurred while updating donation details... Please try again later!');
        }
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.error('An error occurred while updating donation details... Please try again later!');
      }
    );
  }
}
