import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { AdminEditDonationRoutingModule } from "./admin-edit-donation-routing.module";
import { AdminEditDonationComponent } from "./admin-edit-donation.component";

@NgModule({
  imports: [CommonModule, AdminEditDonationRoutingModule, SharedModule],
  declarations: [AdminEditDonationComponent]
})

export class AdminEditDonationModule {}