import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminEditDonationComponent } from "./admin-edit-donation.component";

const routes: Routes = [
    { path: "", component: AdminEditDonationComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminEditDonationRoutingModule { }