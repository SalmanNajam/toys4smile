import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminViewDonationComponent } from "./admin-view-donation.component";

const routes: Routes = [
    { path: "", component: AdminViewDonationComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminViewDonationRoutingModule { }