import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { donationByIdViewModel } from '../../models/donationByIdViewModel';
import { imagePaths } from '../../models/imagePaths';
import { DonationService } from '../../donation.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { User } from '../../models/user.model';
declare var jQuery: any;

@Component({
  selector: 'app-admin-view-donation',
  templateUrl: './admin-view-donation.component.html'
})

export class AdminViewDonationComponent implements OnInit {
  donor: donationByIdViewModel = new donationByIdViewModel();
  imagesList: imagePaths[];
  isLoggedIn: boolean;
  user: User;

  constructor(private donationService: DonationService,
    private route: ActivatedRoute, private location: Location, private spinner: NgxSpinnerService,
    public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    this.user = JSON.parse(localStorage.getItem('user'));
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);
        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
    this.getProductById();
  }

  ngOnInit() {
  }
  // ngOnInit() {
  //   /** spinner starts on init */
  //   this.spinner.show();

  //   setTimeout(() => {
  //       /** spinner ends after 5 seconds */
  //       this.spinner.hide();
  //   }, 5000);
  // }

  getProductById() {
    this.spinner.show();
    const id = +this.route.snapshot.paramMap.get('id');
    this.donationService.getDonationByDonatoinId(id).subscribe(
      (response) => {
        this.spinner.hide();
        this.donor = response;
        this.donationService.getDonationImagesByDonationId(this.donor.DonationId).subscribe(
          (response) => {
            this.spinner.hide();
            this.imagesList = response;
          },
          (errors) => {
            this.spinner.hide();
            this.toastr.error('An error occurred while fetching donation record... Please try again later!');
          }
        )
      },
      (errors) => {
        this.toastr.error('An error occurred while fetching donation record... Please try again later!');
      }
    );
  }
}

