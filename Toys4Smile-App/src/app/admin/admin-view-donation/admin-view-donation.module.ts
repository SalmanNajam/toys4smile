import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { AdminViewDonationRoutingModule } from "./admin-view-donation-routing.module";
import { AdminViewDonationComponent } from "./admin-view-donation.component";

@NgModule({
  imports: [CommonModule, AdminViewDonationRoutingModule, SharedModule],
  declarations: [AdminViewDonationComponent]
})

export class AdminViewDonationModule {}