import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { donationViewModel } from '../../models/donationViewModel';
import { User } from '../../models/user.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DonationService } from '../../donation.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var jQuery: any;

@Component({
    selector: 'app-admin-track-id',
    templateUrl: './admin-track-id.component.html'
})

export class AdminTrackIdComponent implements OnInit {
    trackingid: string;
    trackResult: donationViewModel;
    deleteId: donationViewModel;
    dateOfDonation: string;
    loading: boolean;
    dateString: string = '2018-07-07';
    newDate: Date = new Date(this.dateString);
    donation: donationViewModel = new donationViewModel();
    user: User;
    afterdeleteState = '';
    donations: donationViewModel[];
    searchDonation: FormGroup;
    isLoggedIn: boolean;

    constructor(private fb: FormBuilder, private route: ActivatedRoute,
        private donationService: DonationService, private spinner: NgxSpinnerService,
        private router: Router, public toastr: ToastsManager, vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
        let token = localStorage.getItem('access_token');
        if (token != null) {
            this.isLoggedIn = true;
        } else {
            this.isLoggedIn = false;
        }
        setTimeout(() => {
            (function ($) {
                "use strict";
                var $body = $('body'),
                    $wrapper = $('.body-innerwrapper'),
                    $toggler = $('#offcanvas-toggler'),
                    $close = $('.close-offcanvas'),
                    $offCanvas = $('.offcanvas-menu');
                $toggler.on('click', function (event) {
                    event.preventDefault();
                    stopBubble(event);
                    setTimeout(offCanvasShow, 50);
                });
                $close.on('click', function (event) {
                    event.preventDefault();
                    offCanvasClose();
                });
                var offCanvasShow = function () {
                    $body.addClass('offcanvas');
                    $wrapper.on('click', offCanvasClose);
                    $close.on('click', offCanvasClose);
                    $offCanvas.on('click', stopBubble);
                };
                var offCanvasClose = function () {
                    $body.removeClass('offcanvas');
                    $wrapper.off('click', offCanvasClose);
                    $close.off('click', offCanvasClose);
                    $offCanvas.off('click', stopBubble);
                };
                var stopBubble = function (e) {
                    e.stopPropagation();
                    return true;
                };
                offCanvasClose();
            })(jQuery);
        }, 0);
        this.user = JSON.parse(localStorage.getItem('user'));
        this.createForm();
    }

    createForm() {
        this.searchDonation = this.fb.group({
            trackingId: ['', Validators.required]
        })
    }

    ngOnInit() {
        this.dateOfDonation = '';
        this.trackingid = '';
        this.trackingid = this.route.snapshot.paramMap.get('id');
        this.donationService.getDonationByTrackingId(this.trackingid).subscribe(
            (response) => {
                if (response != null) {
                    this.toastr.success('One donation record found!');
                    this.trackResult = response;
                    this.dateOfDonation = response.dateOfDonation.toString();
                    this.dateOfDonation = this.dateOfDonation.slice(0, 10)
                } else {
                    this.toastr.error('No donation record found against provided Tracking ID!');
                }
            },
            (errors) => {
                this.toastr.error('An error occurred while fetching donation record... Please try again later!');
            }
        );
    }

    onDelete(id: any) {
        this.deleteId = id;
    }

    searchDonations() {
        this.spinner.show();
        const formModel = this.searchDonation.value;
        var searchTrackingId = formModel.trackingId as string;
        this.donationService.getDonationByTrackingId(searchTrackingId).subscribe(
            (response) => {
                this.spinner.hide();
                if (response != null) {
                    this.trackResult = response;
                    this.router.navigate(['/admin/admin-track-id/' + this.trackResult.TrackingId]);
                    this.toastr.success('One donation record found!');
                } else {
                    this.spinner.hide();
                    this.trackResult = null;
                    this.toastr.error('No donation record found against provided Tracking ID!');
                }
            },
            (errors) => {
                this.spinner.hide();
                this.toastr.error('An error occurred while fetching donation record... Please try again later!');
            }
        );
    }

    public onChange1(value1, id: any): void {
        this.donation.DonationId = id.DonationId;
        this.donation.status = value1;
        this.trackingid = id.TrackingId;
        this.spinner.show();
        this.donationService.updateDonationStatus(this.donation).subscribe(
            (response) => {
                this.donationService.getDonationByTrackingId(this.trackingid).subscribe(
                    (response) => {
                        this.spinner.hide();
                        this.trackResult = response;
                        this.toastr.success('The donation has been changed successfully!');
                    },
                    (errors) => {
                        this.spinner.hide();
                        this.toastr.error('Unable to fetch donations and change donation status... Please try again later!');
                    }
                );
            },
            (errors) => {
                this.spinner.hide();
                this.toastr.error('Unable to change donation status... Please try again later!');
            }
        );
    }

    deleteDonation() {
        this.spinner.show();
        this.donationService.deleteDonation(this.deleteId).subscribe(
            (response) => {
                if (response !== '') {
                    this.spinner.hide();
                    this.toastr.success('The donation has been deleted successfully!');
                    this.afterdeleteState = response;
                    this.donationService.getAllDonationByStatus(this.afterdeleteState).subscribe(
                        (response) => {
                            this.router.navigate(['/admin/admin-donations']);
                        },
                        (errors) => {
                            this.spinner.hide();
                            this.toastr.error('Unable to fetch donations... Please try again later!');
                        }
                    );
                }
            },
            (errors) => {
                this.spinner.hide();
                this.toastr.error('An error occurred while deleting donation... Please try again later!');
            }
        );
    }
}
