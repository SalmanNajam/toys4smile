import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { AdminTrackIdRoutingModule } from "./admin-track-id-routing,module";
import { AdminTrackIdComponent } from "./admin-track-id.component";

@NgModule({
  imports: [CommonModule, AdminTrackIdRoutingModule, SharedModule],
  declarations: [AdminTrackIdComponent]
})

export class AdminTrackIdModule {}