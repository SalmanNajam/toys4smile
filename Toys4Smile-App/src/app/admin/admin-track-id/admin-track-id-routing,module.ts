import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminTrackIdComponent } from "./admin-track-id.component";

const routes: Routes = [
    { path: "", component: AdminTrackIdComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminTrackIdRoutingModule { }