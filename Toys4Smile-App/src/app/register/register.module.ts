import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { RegisterRoutingModule } from "./register-routing.module";
import { RegisterComponent } from "./register.component";
import { AdminModule } from "../admin/admin.module";

@NgModule({
  imports: [CommonModule, RegisterRoutingModule, SharedModule, AdminModule],
  declarations: [RegisterComponent]
})

export class RegisterModule {}