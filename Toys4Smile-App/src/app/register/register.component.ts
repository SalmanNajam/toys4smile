import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { registerUser } from '../Models/registerUser';
import { UserServicee } from '../user.servicee';
import { Router } from '../../../node_modules/@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Email } from '../models/email.model';
import { User } from '../models/user.model';
declare var jQuery: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  isLoggedIn: boolean;
  userr: User;
  numberPrefix: string;
  email: Email = new Email();
  public emailInUse: boolean;
  public isEmailAvailable: boolean;
  constructor(private fb: FormBuilder, private userService: UserServicee,
    private router: Router,
    private spinner: NgxSpinnerService) {
    this.userr = JSON.parse(localStorage.getItem('user'));
    this.createForm();
    this.numberPrefix = '';
    this.emailInUse = false;
    this.isEmailAvailable = false;
    let token = localStorage.getItem('access_token');
    if (token != null) {
        this.isLoggedIn = true;
    } else {
        this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);

        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
  }
  user: registerUser = new registerUser();
  registerForm: FormGroup;
  ngOnInit() { }
  createForm() {
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required]
      , lastName: ['', Validators.required]
      , email: ['', [Validators.required, Validators.email]]
      , dob: ['']
      , mobile: ['', [Validators.required, Validators.maxLength(10)]]
      , countrycode: ['', [Validators.required, Validators.maxLength(5)]]
      , password: ['', [Validators.required, Validators.minLength(8)]]
      , address: ['', Validators.required]
    })
  }

  onChangeNumberPrefix(nmbrPrefix: string) {
    this.numberPrefix = nmbrPrefix;
  }

  focusOutFunction() {
    this.email.emailId = this.registerForm.value.email;
    this.userService.hasEmailRegistered(this.email).subscribe(
      (response) => {
        if (response === true) {
          this.isEmailAvailable = true;
          this.registerForm.controls["email"].setValue('');
        } else if (response === false) {
          this.isEmailAvailable = false;
          console.log("false");
        }
      },
      (errors) => {
        console.log(errors);
      }
    )
  }

  registerUser() {
    this.spinner.show();
    const formModel = this.registerForm.value;
    this.user.firstName = formModel.firstName;
    this.user.lastName = formModel.lastName;
    this.user.email = formModel.email;
    this.user.mobileNo = formModel.mobile;
    this.user.dob = formModel.dob;
    this.user.password = formModel.password;
    this.user.address = formModel.address;
    this.user.countrycode = formModel.countrycode;
    this.userService.registerUser(this.user).subscribe(
      (response) => {
        if (response === true) {
          this.createForm();
          this.spinner.hide();
          this.router.navigate(['/registrationsuccessful']);
        } else {
          this.spinner.hide();
        }
      },
      (errors) => {
        this.spinner.hide();
      }
    );
  }
}
