import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomeModule' },
  { path: 'aboutus', loadChildren: './aboutus/aboutus.module#AboutUsModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterModule' },
  { path: 'orphanage', loadChildren: './orphanage/orphanage.module#OrphanageModule' },
  { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
  { path: 'signin', loadChildren: './signin/signin.module#SignInModule' },
  { path: 'guest', loadChildren: './guest/guest.module#GuestModule' },
  { path: 'track', loadChildren: './track/track.module#TrackModule' },
  { path: 'donor', loadChildren: './donor/donor.module#DonorModule' },
  { path: 'donate', loadChildren: './donate/donate.module#DonateModule' },
  { path: 'registrationsuccessful', loadChildren: './registrationsuccessful/registrationsuccessful.module#RegistrationSuccessfulModule' },
  { path: 'donationsuccessful/:id', loadChildren: './donationsuccessful/donationsuccessful.module#DonationSuccessfulModule' },
  { path: 'forgotpassword', loadChildren: './forgotpassword/forgotpassword.module#ForgotPasswordModule' },
  { path: 'disabled-user', loadChildren: './disabled-user/disabled-user.module#DisabledUserModule'  },
  { path: 'view-donation/:id', loadChildren: './view-donation/view-donation.module#ViewDonationModule' },
  { path: 'logout', component: LogoutComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
