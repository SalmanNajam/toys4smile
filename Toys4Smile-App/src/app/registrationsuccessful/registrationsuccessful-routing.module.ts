import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RegistrationsuccessfulComponent } from "./registrationsuccessful.component";

const routes: Routes = [
    { path: "", component: RegistrationsuccessfulComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class RegistrationSuccessfulRoutingModule { }