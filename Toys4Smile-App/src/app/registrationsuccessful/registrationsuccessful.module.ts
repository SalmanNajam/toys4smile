import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { RegistrationSuccessfulRoutingModule } from "./registrationsuccessful-routing.module";
import { RegistrationsuccessfulComponent } from "./registrationsuccessful.component";

@NgModule({
  imports: [CommonModule, RegistrationSuccessfulRoutingModule, SharedModule],
  declarations: [RegistrationsuccessfulComponent]
})

export class RegistrationSuccessfulModule { }