import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OrphanageDashboardComponent } from "./orphanage-dashboard.component";

const routes: Routes = [
    { path: '', component: OrphanageDashboardComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class OrphanageDashboardRoutingModule { }