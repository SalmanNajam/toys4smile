import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { donationViewModel } from '../../models/donationViewModel';
import { User } from '../../models/user.model';
import { DonationService } from '../../donation.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var jQuery: any;

@Component({
  selector: 'app-orphanage-dashboard',
  templateUrl: './orphanage-dashboard.component.html'
})
export class OrphanageDashboardComponent implements OnInit {

  donations: donationViewModel[];
  donation: donationViewModel = new donationViewModel();
  deleteId: donationViewModel;
  ID: number;
  donationStatus = 'All';
  searchStatus: string = "";
  previousStatus = '';
  afterdeleteState = '';
  isLoggedIn: boolean;
  user: User;
  trackResult: donationViewModel;
  trackingidwithuserid: string;

  constructor(private fb: FormBuilder, private donationService: DonationService,
    private userService: UserService, public toastr: ToastsManager, vcr: ViewContainerRef,
    private spinner: NgxSpinnerService,
    private router: Router) {
    this.toastr.setRootViewContainerRef(vcr);
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');

        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });

        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });

        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);

        };

        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };

        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };

        offCanvasClose();

      })(jQuery);

    }, 0);
    this.user = JSON.parse(localStorage.getItem('user'));
    this.spinner.show();
    this.donationService.getAllDonationsByOrphanageId(this.user.UserId).subscribe(
      (response) => {
        this.spinner.hide();
        this.donations = response;
      },
      (errors) => {
        this.spinner.hide();
      }
    );
    this.createForm();
  }

  searchDonation: FormGroup;

  createForm() {
    this.searchDonation = this.fb.group({
      trackingId: ['', Validators.required]
    })
  }

  public onChange(value): void {
    this.spinner.show();
    this.donationStatus = value;
    this.searchStatus = value;
    if (value === 'All') {
      this.donationService.getAllDonationsByOrphanageId(this.user.UserId).subscribe(
        (response) => {
          this.spinner.hide();
          this.donations = response;
        },
        (errors) => {
          this.spinner.hide();
        }
      );
    } else {
      this.donationService.getAllReceivedDonationsForOrphanage(value + ',' + this.user.UserId).subscribe(
        (response) => {
          this.spinner.hide();
          this.donations = response;
        },
        (errors) => {
          this.spinner.hide();
        }
      );
    }
  }

  public onChange1(value1, id: any): void {
    this.previousStatus = this.donationStatus;
    this.donation.DonationId = id.DonationId;
    this.donation.status = value1;
    this.spinner.show();
    this.donationService.updateDonationStatus(this.donation).subscribe(
      (response) => {
        this.donationService.getAllDonationsByOrphanageId(this.user.UserId).subscribe(
          (response) => {
            this.spinner.hide();
            this.donations = response;
            this.toastr.success('The donation has been received successfully!');
          },
          (errors) => {
            this.spinner.hide();
            this.toastr.error('Unable to fetch donations... Please try again later!');
          }
        );
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.error('An error occurred while changing donation status... Please try again later!');
      }
    );
  }

  getDonationId(id: number) {
    this.ID = id;
  }

  onDelete(id: any) {
    this.deleteId = id;
  }

  deleteDonation() {
    this.spinner.show();
    this.donationService.deleteDonation(this.deleteId).subscribe(
      (response) => {
        if (response !== '') {
          this.spinner.hide();
          this.toastr.success('The donation has been deleted successfully!');
          this.afterdeleteState = response;
          this.donationService.getAllDonationByStatus(this.afterdeleteState).subscribe(
            (response) => {
              this.spinner.hide();
              this.donations = response;
            },
            (errors) => {
              this.spinner.hide();
              this.toastr.error('Unable to fetch donations... Please try again later!');
            }
          );
        }
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.error('An error occurred while deleting donation... Please try again later!');
      }
    );
  }

  ngOnInit() {
  }

  searchDonations() {
    this.spinner.show();
    const formModel = this.searchDonation.value;
    var searchTrackingId = formModel.trackingId as string;
    this.trackingidwithuserid = searchTrackingId + ',' + this.user.UserId;
    this.donationService.getAllDonationByTrackingId(this.trackingidwithuserid).subscribe(
      (response) => {
        this.spinner.hide();
        if (response != null) {
          this.toastr.success('One donation found!');
          this.trackResult = response;
          this.router.navigate(['orphanage/orphanage-track-id/' + this.trackResult.TrackingId]);
        } else {
          this.spinner.hide();
          this.toastr.error('No donation record found against provided Tracking ID!');
        }
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.error('An error occurred while fetching donation details... Please try again later!');
      }
    );
  }
}
