import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { OrphanageDashboardRoutingModule } from "./orphanage-dashboard-routing.module";
import { OrphanageDashboardComponent } from "./orphanage-dashboard.component";

@NgModule({
  imports: [CommonModule, OrphanageDashboardRoutingModule, SharedModule],
  declarations: [OrphanageDashboardComponent]
})

export class OrphanageDashboardModule {}