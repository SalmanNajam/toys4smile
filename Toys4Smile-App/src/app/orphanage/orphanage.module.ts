import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrphanageRoutingModule } from './orphanage-routing.module';
import { OrphanageComponent } from './orphanage.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    OrphanageRoutingModule,
    SharedModule
  ],
  declarations: [OrphanageComponent]
})
export class OrphanageModule { }
