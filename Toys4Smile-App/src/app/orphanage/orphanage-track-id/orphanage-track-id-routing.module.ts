import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OrphanageTrackIdComponent } from "./orphanage-track-id.component";

const routes: Routes = [
    { path: '', component: OrphanageTrackIdComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class OrphanageTrackIdRoutingModule { }