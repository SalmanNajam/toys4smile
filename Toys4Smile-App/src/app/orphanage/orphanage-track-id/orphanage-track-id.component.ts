import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../models/user.model';
import { donationViewModel } from '../../models/donationViewModel';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DonationService } from '../../donation.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var jQuery: any;

@Component({
    selector: 'app-orphanage-track-id',
    templateUrl: './orphanage-track-id.component.html'
})

export class OrphanageTrackIdComponent implements OnInit {
    trackingid: string;
    user: User;
    trackResult: donationViewModel;
    dateOfDonation: string;
    loading: boolean;
    dateString: string = '2018-07-07';
    newDate: Date = new Date(this.dateString);
    donation: donationViewModel = new donationViewModel();
    trackingidwithuserid: string;
    isLoggedIn: boolean;
    searchDonation: FormGroup;

    constructor(private fb: FormBuilder, private route: ActivatedRoute,
        private donationService: DonationService,
        private spinner: NgxSpinnerService, public toastr: ToastsManager, vcr: ViewContainerRef,
        private router: Router) {
        this.toastr.setRootViewContainerRef(vcr);
        let token = localStorage.getItem('access_token');
        if (token != null) {
            this.isLoggedIn = true;
        } else {
            this.isLoggedIn = false;
        }
        setTimeout(() => {
            (function ($) {
                "use strict";
                var $body = $('body'),
                    $wrapper = $('.body-innerwrapper'),
                    $toggler = $('#offcanvas-toggler'),
                    $close = $('.close-offcanvas'),
                    $offCanvas = $('.offcanvas-menu');
                $toggler.on('click', function (event) {
                    event.preventDefault();
                    stopBubble(event);
                    setTimeout(offCanvasShow, 50);
                });
                $close.on('click', function (event) {
                    event.preventDefault();
                    offCanvasClose();
                });
                var offCanvasShow = function () {
                    $body.addClass('offcanvas');
                    $wrapper.on('click', offCanvasClose);
                    $close.on('click', offCanvasClose);
                    $offCanvas.on('click', stopBubble);
                };
                var offCanvasClose = function () {
                    $body.removeClass('offcanvas');
                    $wrapper.off('click', offCanvasClose);
                    $close.off('click', offCanvasClose);
                    $offCanvas.off('click', stopBubble);
                };
                var stopBubble = function (e) {
                    e.stopPropagation();
                    return true;
                };
                offCanvasClose();
            })(jQuery);
        }, 0);
        this.createForm();
    }

    createForm() {
        this.searchDonation = this.fb.group({
            trackingId: ['', Validators.required]
        })
    }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.dateOfDonation = '';
        this.trackingid = '';
        this.trackingid = this.route.snapshot.paramMap.get('id');
        this.trackingidwithuserid = this.trackingid + ',' + this.user.UserId;
        this.donationService.getAllDonationByTrackingId(this.trackingidwithuserid).subscribe(
            (response) => {
                if (response != null) {
                    this.toastr.success('One record found!');
                    this.trackResult = response;
                    this.dateOfDonation = response.dateOfDonation.toString();
                    this.dateOfDonation = this.dateOfDonation.slice(0, 10);
                } else {
                    this.toastr.error('No record found against provided Tracking ID!');
                }
            },
            (errors) => {
                this.toastr.error('An error occurred while fetching donation details... Please try again later!');
            }
        );
    }

    searchDonations() {
        this.spinner.show();
        const formModel = this.searchDonation.value;
        var searchTrackingId = formModel.trackingId as string;
        this.donationService.getAllDonationByTrackingId(searchTrackingId).subscribe(
            (response) => {
                this.spinner.hide();
                if (response != null) {
                    this.toastr.success('One record found!');
                    this.trackResult = response;
                    this.router.navigate(['orphanage/orphanage-track-id/' + this.trackResult.TrackingId]);
                } else {
                    this.spinner.hide();
                    this.toastr.error('No record found against provided Tracking ID!');
                    this.trackResult = null;
                    this.spinner.hide();
                }
            },
            (errors) => {
                this.spinner.hide();
                this.toastr.error('An error occurred while fetching donation details... Please try again later!');
            }
        );
    }

    public onChange1(value1, id: any): void {
        this.donation.DonationId = id.DonationId;
        this.donation.status = value1;
        this.trackingid = id.TrackingId;
        this.spinner.show();
        this.donationService.updateDonationStatus(this.donation).subscribe(
            (response) => {
                this.toastr.success('The donation has been received successfully!');
                setTimeout(() => {
                    this.router.navigate(['orphanage/orphanage-dashboard']);
                }, 2000);
            },
            (errors) => {
                this.spinner.hide();
                this.toastr.error('An error occurred while changing donation status... Please try again later!');
            }
        );
    }
}
