import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { OrphanageTrackIdRoutingModule } from "./orphanage-track-id-routing.module";
import { OrphanageTrackIdComponent } from "./orphanage-track-id.component";

@NgModule({
    imports: [CommonModule, OrphanageTrackIdRoutingModule, SharedModule],
    declarations: [OrphanageTrackIdComponent]
})

export class OrphanageTrackIdModule { }