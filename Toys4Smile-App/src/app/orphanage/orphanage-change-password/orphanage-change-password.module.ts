import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { OrphanageChangePasswordRoutingModule } from "./orphanage-change-password-routing.module";
import { OrphanageChangePasswordComponent } from "./orphanage-change-password.component";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
  imports: [CommonModule, OrphanageChangePasswordRoutingModule, SharedModule],
  declarations: [OrphanageChangePasswordComponent]
})

export class OrphanageChangePasswordModule {}