import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OrphanageChangePasswordComponent } from "./orphanage-change-password.component";

const routes: Routes = [
    { path: '', component: OrphanageChangePasswordComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class OrphanageChangePasswordRoutingModule { }