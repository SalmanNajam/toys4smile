import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from '../../models/user.model';
import { ChangePassword } from '../../models/changePasswordModel';
import { UserService } from '../../services/user.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var jQuery: any;

@Component({
  selector: 'app-orphanage-change-password',
  templateUrl: './orphanage-change-password.component.html'
})

export class OrphanageChangePasswordComponent implements OnInit {
  user: User;
  form: FormGroup;
  isLoggedIn: boolean;
  returnUrlDonor: string;
  returnUrlAdmin: string;
  returnUrl: string;
  notice: boolean;
  redirect: boolean;
  changePassword: ChangePassword = new ChangePassword();

  constructor(private userService: UserService, private router: Router,
    public toastr: ToastsManager, vcr: ViewContainerRef,
    private spinner: NgxSpinnerService) {
    this.toastr.setRootViewContainerRef(vcr);
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');

        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);

        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
    this.user = JSON.parse(localStorage.getItem('user'));
    this.notice = true;
    this.redirect = false;
  }

  ngOnInit() {
    this.form = new FormGroup({
      oldPassword: new FormControl('', [Validators.required, Validators.minLength(8)]),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(8)])
    })
  }

  onChangePassword() {
    this.spinner.show();
    this.changePassword.oldPassword = this.form.value.oldPassword;
    this.changePassword.newPassword = this.form.value.newPassword;
    this.changePassword.userId = this.user.UserId;
    this.changePassword.roleId = this.user.RoleId;

    this.userService.onChangePassword(this.changePassword).subscribe(
      (response) => {
        this.spinner.hide();
        if (response === 0) {
          this.toastr.error('Your current password did not match!');
        } else if (response === 1) {
          this.toastr.success('Your password has been changed successfully!');
          this.notice = false;
          this.redirect = true;
          setTimeout(() => {
            localStorage.removeItem('access_token');
            localStorage.removeItem('expires');
            localStorage.removeItem('user');
            this.router.navigate(['/signin']);
          }, 2000);
        } else if (response === 2) {
          this.toastr.error('No user exists!');
        }
      },
      (errors) => {
        this.spinner.hide();
        this.toastr.error('An error occurred while updating password... Please try again later!');
      }
    );
  }
}
