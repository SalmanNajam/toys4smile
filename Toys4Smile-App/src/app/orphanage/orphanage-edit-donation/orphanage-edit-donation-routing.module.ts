import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OrphanageEditDonationComponent } from "./orphanage-edit-donation.component";

const routes: Routes = [
    { path: '', component: OrphanageEditDonationComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class OrphanageEditDonationRoutingModule { }