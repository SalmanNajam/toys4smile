import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { OrphanageEditDonationRoutingModule } from "./orphanage-edit-donation-routing.module";
import { OrphanageEditDonationComponent } from "./orphanage-edit-donation.component";

@NgModule({
  imports: [CommonModule, OrphanageEditDonationRoutingModule, SharedModule],
  declarations: [OrphanageEditDonationComponent]
})

export class OrphanageEditDonationModule {}