import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrphanageComponent } from './orphanage.component';
import { AuthGuard } from '../core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '', component: OrphanageComponent, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'orphanage-dashboard', pathMatch: 'full' },
      { path: 'orphanage-dashboard', loadChildren: './orphanage-dashboard/orphanage-dashboard.module#OrphanageDashboardModule', canActivate: [AuthGuard] },
      { path: 'orphanage-edit-donation/:id', loadChildren: './orphanage-edit-donation/orphanage-edit-donation.module#OrphanageEditDonationModule', canActivate: [AuthGuard] },
      { path: 'orphanage-track-id/:id', loadChildren: './orphanage-track-id/orphanage-track-id.module#OrphanageTrackIdModule', canActivate: [AuthGuard] },
      { path: 'orphanage-change-password', loadChildren: './orphanage-change-password/orphanage-change-password.module#OrphanageChangePasswordModule', canActivate: [AuthGuard] }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class OrphanageRoutingModule { }
