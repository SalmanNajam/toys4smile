import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ViewDonationComponent } from "./view-donation.component";

const routes: Routes = [
    { path: "", component: ViewDonationComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class ViewDonationRoutingModule { }