import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { donationByIdViewModel } from '../models/donationByIdViewModel';
import { imagePaths } from '../models/imagePaths';
import { DonationService } from '../donation.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from '../models/user.model';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var jQuery: any;

@Component({
  selector: 'app-view-donation',
  templateUrl: './view-donation.component.html'
})

export class ViewDonationComponent implements OnInit {
  donor: donationByIdViewModel = new donationByIdViewModel();
  imagesList: imagePaths[];
  user: User;
  isLoggedIn: boolean;
  public imagesUrl;

  constructor(private donationService: DonationService,
    private route: ActivatedRoute, private location: Location, private spinner: NgxSpinnerService,
    public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    this.getProductById();
    this.user = JSON.parse(localStorage.getItem('user'));
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);
        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
  }

  ngOnInit() { }

  // ngOnInit() {
  //   /** spinner starts on init */
  //   this.spinner.show();
  //   setTimeout(() => {
  //       /** spinner ends after 5 seconds */
  //       this.spinner.hide();
  //   }, 5000);
  // }

  getProductById() {
    debugger;
    this.spinner.show();
    const id = +this.route.snapshot.paramMap.get('id');
    this.donationService.getDonationByDonatoinId(id).subscribe(
      (response) => {
        this.spinner.hide();
        this.donor = response;
        console.log(this.donor);
        this.donationService.getDonationImagesByDonationId(this.donor.DonationId).subscribe(
          (response) => {
            this.spinner.hide();
            this.imagesList = response;
          },
          (errors) => {
            this.spinner.hide();
            this.toastr.error('An error occurred while fetching donation record... Please try again later!');
          }
        )
      },
      (errors) => {
        this.toastr.error('An error occurred while fetching donation record... Please try again later!');
      }
    );
  }
}
