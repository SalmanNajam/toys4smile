import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { ViewDonationComponent } from "./view-donation.component";
import { ViewDonationRoutingModule } from "./view-donation-routing.module";

@NgModule({
    imports: [CommonModule, ViewDonationRoutingModule, SharedModule],
    declarations: [ViewDonationComponent]
})

export class ViewDonationModule { }