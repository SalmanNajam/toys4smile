import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DonorComponent } from "./donor.component";
import { AuthGuard } from "../core/services/auth-guard.service";

const routes: Routes = [
    { path: "", component: DonorComponent, canActivate: [AuthGuard] }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class DonorRoutingModule { }
