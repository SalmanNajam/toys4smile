import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { DonorRoutingModule } from "./donor-routing.module";
import { DonorComponent } from "./donor.component";

@NgModule({
  imports: [CommonModule, DonorRoutingModule, SharedModule],
  declarations: [DonorComponent]
})

export class DonorModule { }