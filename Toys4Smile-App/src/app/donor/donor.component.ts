import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { UserServicee } from '../user.servicee';
import { User } from '../Models/user.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '../../../node_modules/@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { donationViewModel } from '../models/donationViewModel';
import { DonationService } from '../donation.service';
import { OrphanageTitles } from '../models/orphanageTitles.model';
import { ImageCompressService, ImageUtilityService, IImage } from "ng2-image-compress";
declare var jQuery: any;
let images: Array<IImage> = [];

@Component({
  selector: 'app-donor',
  templateUrl: './donor.component.html'
})

export class DonorComponent implements OnInit {
  user: User;
  isLoggedIn: boolean;
  registeredDonorForm: FormGroup; private
  public loading: boolean;
  trackingid: string;
  userDonations: donationViewModel[];
  orphanageTitles: OrphanageTitles[];
  showTitle: boolean = false;
  ngoId: string;
  files: any;
  selectedFile: File[] = [];
  urls = new Array<string>();
  selectedImage: any;
  processedImages: any = [];

  constructor(private fb: FormBuilder, private users: UserServicee, private userService: UserService, private router: Router,
    private spinner: NgxSpinnerService, private donationService: DonationService, private imgCompressService: ImageCompressService) {
    this.createForm();
    this.loading = false;
    this.user = JSON.parse(localStorage.getItem('user'));
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);
        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
  }

  ngOnInit() {
    this.userService.GetOrphanageTitles().subscribe(
      (response) => {
        this.orphanageTitles = response;
      },
      (errors) => {
        console.log(errors);
      }
    )
    this.donationService.getDonationsById(this.user.UserId).subscribe(
      (response) => {
        this.userDonations = response;
      },
      (errors) => {
      }
    );
  }

  createForm() {
    this.registeredDonorForm = this.fb.group({
      noOfToys: ['', Validators.required]
      , orphanageTitle: ['', Validators.required]
      , specialComments: ['']
    })
  }

  imageOptimizied(event) {
    this.processedImages = [];
    let files: File[];
    let observer = ImageUtilityService.filesToSourceImages(event.target.files);
    observer.subscribe((image) => {
      images.push(image);
    }, null, () => {
      var files: IImage[] = images;
      let image_files: IImage[] = [];
      let nonImage_files: IImage[] = [];
      for (var i = 0; i < files.length; i++) {
        if (String(files[i].imageDataUrl).toUpperCase().indexOf("DATA:IMAGE/PNG;") !== -1 ||
          String(files[i].imageDataUrl).toUpperCase().indexOf("DATA:IMAGE/JPEG;") !== -1) {
          image_files.push(files[i]);
        }
        else {
          nonImage_files.push(files[i]);
        }
      };
      ImageCompressService.IImageListToCompressedImageSource(images).then(imagesResult => {
        this.processedImages = imagesResult;
      })
    });
  }

  deleteImage(item: any) {
    for (var i = this.processedImages.length - 1; i >= 0; i--) {
      if (this.processedImages[i] == item) {
       
        this.processedImages.splice(i, 1);
      }
    }
  }

  addDonation() {
    this.spinner.show();
    const formModel = this.registeredDonorForm.value;
    let formData: FormData = new FormData();
    let oneFile = this.selectedFile[0];
    formData.append('noOfToys', formModel.noOfToys);
    formData.append('userId', this.user.UserId.toString());
    formData.append('useremail', this.user.email);
    formData.append('userfirstname', this.user.firstName);
    formData.append('selectedOrphanageId', formModel.orphanageTitle);
    formData.append('specialComments', formModel.specialComments);
    for (var i = 0; i < this.processedImages.length; i++) {
      var blob = new Blob([this.processedImages[i].compressedImage.imageDataUrl], { type: 'image/jpg' });
      formData.append('MyFile' + i, blob, this.processedImages[i].compressedImage.imageDataUrl);
    }
    this.users.donateAsRegisteredUser(formData).subscribe(
      (response) => {

        this.trackingid = response;
        if (this.trackingid !== "") {
          this.spinner.hide();
          this.router.navigate(['/donationsuccessful/' + this.trackingid]);
        } else {
          this.spinner.hide();
        }
      },
      (errors) => {
        this.loading = false;
      }
    );
  }
}
