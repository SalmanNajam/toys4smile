import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserServicee } from '../user.servicee';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { OrphanageTitles } from '../models/orphanageTitles.model';
import { ImageCompressService, ImageUtilityService, IImage } from "ng2-image-compress";
import { User } from '../models/user.model';
let images: Array<IImage> = [];
declare var jQuery: any;

@Component({
  selector: 'app-guest',
  templateUrl: './guest.component.html'
})

export class GuestComponent implements OnInit {
  orphanageTitles: OrphanageTitles[];
  isLoggedIn: boolean;
  trackingid: string;
  numberPrefix: string;
  user: User;
  guestDonorForm: FormGroup;
  files: any;
  selectedFile: File[] = [];
  urls = new Array<string>();
  selectedImage: any;
  processedImages: any = [];

  constructor(private fb: FormBuilder, private userServicee: UserServicee, private userService: UserService, private router: Router,
    private spinner: NgxSpinnerService, private imgCompressService: ImageCompressService) {
    this.createForm();
    this.numberPrefix = '';
    this.user = JSON.parse(localStorage.getItem('user'));
    let token = localStorage.getItem('access_token');
    if (token != null) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    setTimeout(() => {
      (function ($) {
        "use strict";
        var $body = $('body'),
          $wrapper = $('.body-innerwrapper'),
          $toggler = $('#offcanvas-toggler'),
          $close = $('.close-offcanvas'),
          $offCanvas = $('.offcanvas-menu');
        $toggler.on('click', function (event) {
          event.preventDefault();
          stopBubble(event);
          setTimeout(offCanvasShow, 50);
        });
        $close.on('click', function (event) {
          event.preventDefault();
          offCanvasClose();
        });
        var offCanvasShow = function () {
          $body.addClass('offcanvas');
          $wrapper.on('click', offCanvasClose);
          $close.on('click', offCanvasClose);
          $offCanvas.on('click', stopBubble);
        };
        var offCanvasClose = function () {
          $body.removeClass('offcanvas');
          $wrapper.off('click', offCanvasClose);
          $close.off('click', offCanvasClose);
          $offCanvas.off('click', stopBubble);
        };
        var stopBubble = function (e) {
          e.stopPropagation();
          return true;
        };
        offCanvasClose();
      })(jQuery);
    }, 0);
  }

  ngOnInit() {
    this.userService.GetOrphanageTitles().subscribe(
      (response) => {
        this.orphanageTitles = response;
      },
      (errors) => {
        console.log(errors);
      }
    )
  }

  createForm() {
    this.guestDonorForm = this.fb.group({
      fullName: ['']
      , email: ['', [Validators.required, Validators.email]]
      , mobile: ['', Validators.required]
      , noOfToys: ['', Validators.required]
      , address: ['', Validators.required]
      , countrycode: ['', Validators.required]
      , orphanageTitle: ['', Validators.required]
      , specialComments: ['']
    })
  }

  imageOptimizied(event) {
    this.processedImages = [];
    let files: File[];
    let observer = ImageUtilityService.filesToSourceImages(event.target.files);
    observer.subscribe((image) => {
      images.push(image);
    }, null, () => {
      var files: IImage[] = images;
      let image_files: IImage[] = [];
      let nonImage_files: IImage[] = [];
      for (var i = 0; i < files.length; i++) {
        if (String(files[i].imageDataUrl).toUpperCase().indexOf("DATA:IMAGE/PNG;") !== -1 ||
          String(files[i].imageDataUrl).toUpperCase().indexOf("DATA:IMAGE/JPEG;") !== -1) {
          image_files.push(files[i]);
        }
        else {
          nonImage_files.push(files[i]);
        }
      };
      ImageCompressService.IImageListToCompressedImageSource(images).then(imagesResult => {
        this.processedImages = imagesResult;
      })
    });
  }

  deleteImage(item: any) {
    for (var i = this.processedImages.length - 1; i >= 0; i--) {
      if (this.processedImages[i] == item) {
       
        this.processedImages.splice(i, 1);
      }
    }
  }

  onChangeNumberPrefix(nmbrPrefix: string) {
    this.numberPrefix = nmbrPrefix;
  }

  addDonation() {
    this.spinner.show();
    const formModel = this.guestDonorForm.value;
    let formData: FormData = new FormData();
    formData.append('fullName', formModel.fullName as string);
    formData.append('mobileNumber', formModel.mobile);
    formData.append('noOfToys', formModel.noOfToys);
    formData.append('email', formModel.email);
    formData.append('address', formModel.address);
    formData.append('countrycode', formModel.countrycode);
    formData.append('selectedOrphanageId', formModel.orphanageTitle);
    formData.append('specialComments', formModel.specialComments);
    for (var i = 0; i < this.processedImages.length; i++) {
      var blob = new Blob([this.processedImages[i].compressedImage.imageDataUrl], { type: 'image/jpg' });
      formData.append('MyFile' + i, blob, this.processedImages[i].compressedImage.imageDataUrl);
    }
    this.userServicee.donateAsGuest(formData).subscribe(
      (response) => {
        if (response !== "") {
          this.spinner.hide();
          this.trackingid = response;
          this.createForm();
          this.router.navigate(['/donationsuccessful/' + this.trackingid]);
        } else {
          this.spinner.hide();
        }
      },
      (errors) => {
        this.spinner.hide();
      }
    );
  }
}