import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { GuestComponent } from "./guest.component";
import { GuestRoutingModule } from "./guest-routing.module";

@NgModule({
  imports: [CommonModule, GuestRoutingModule, SharedModule],
  declarations: [GuestComponent]
})

export class GuestModule {}