import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GuestComponent } from "./guest.component";

const routes: Routes = [
    { path: "", component: GuestComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class GuestRoutingModule { }