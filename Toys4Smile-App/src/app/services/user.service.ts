import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { Email } from '../models/email.model';
import { adminUserModel } from '../models/adminUserModel';
import { ChangePassword } from '../models/changePasswordModel';

@Injectable()

export class UserService {
  constructor(private httpClient: HttpClient) { }

  onSignIn(email: string, password: string): Observable<any> {
    let reqheaders = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    var data = "username=" + encodeURIComponent(email) + "&password=" + encodeURIComponent(password) + "&grant_type=password";
    return this.httpClient.post<any>('http://api.toys4smile.com/token', data, { headers: reqheaders });
  }

  onChangePassword(value: ChangePassword): Observable<number> {
    return this.httpClient.post<number>('http://api.toys4smile.com/api/Orphanage/ChangePassword', value);
  }

  getDonorDetails(): Observable<User> {
    return this.httpClient.get<User>('http://api.toys4smile.com/api/GetDonorClaims'
      , { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('access_token') }) });
  }

  isAuthorized(): Observable<boolean> {
    return this.httpClient.get<boolean>('http://api.toys4smile.com/api/IsAuthorized'
      , { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('access_token') }) });
  }

  forgotPassword(email: Email): Observable<boolean> {
    return this.httpClient.post<boolean>('http://api.toys4smile.com/api/Donor/ForgotPassword/', email);
  }

  getAllUsersByRoleId(roleId: number): Observable<User[]> {
    return this.httpClient.get<User[]>('http://api.toys4smile.com/api/Donor/getAllUsersByRoleId/' + roleId);
  }

  deleteUser(value: User): Observable<boolean> {
    return this.httpClient.post<boolean>('http://api.toys4smile.com/api/Donor/deleteUser', value);
  }

  getUserByUserId(userId: number): Observable<adminUserModel> {
    return this.httpClient.get<adminUserModel>('http://api.toys4smile.com/api/Donor/getUserById/' + userId);
  }

  changeUserStatus(user: User): Observable<boolean> {
    return this.httpClient.post<boolean>('http://api.toys4smile.com/api/Admin/ChangeUserStatus/', user);
  }

  GetUserByEmail(email: Email): Observable<User> {
    return this.httpClient.post<User>('http://api.toys4smile.com/api/Donor/GetUserByEmail', email);
  }

  GetOrphanageTitles(): Observable<any> {
    return this.httpClient.get<any>('http://api.toys4smile.com/api/Orphanage/GetOrphanageTitle');

  }


}
